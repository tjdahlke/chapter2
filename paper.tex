
% \chapter{Level set Hessian and radial basis functions}
\chapter{Level sets and shape optimization}

\label{chap:chap2}

\par In the previous chapter I reviewed the problems with modeling salt and how level sets are an ideal tool for addressing some of those challenges. In this chapter I explain the basics of level sets and how they apply to the shape optimization problem of finding a salt boundary that minimizes the FWI objective function. I derive the gradient and demonstrate updating on a simple 2D model. Last, I derive the Hessian equations and demonstrate updating on 2D models with a comparison between the full Hessian and the Gauss-Newton Hessian approaches.

\section{Level sets as a tool}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{Basic definitions}

A level set is a contour of a higher dimensional surface, $\phi$. One can use the level set as an elegant tool for keeping track of boundaries as they change form. If one considers a 2D level set being the contour of a 3D implicit surface, then the spatial domain of the 2D level set can be defined as $\Theta \subset \mathbf{R^{2}}$ with elements $\chi \in \Theta$. A salt body $\Omega$ is simply a subset of the 2D domain ($\Omega \subset \Theta$) such that:

$\Omega=\{ \chi \mid \phi(\chi,\tau)>0 \}$,

\noindent where $\tau$ indicates the `time' axis along which the evolution steps progress ($\tau=0$ is the initial iteration). As such, for a single updating step (along $\tau$), our current salt body $\Omega$ evolves to $\Omega^{'}$. I further define the boundary of the salt body as $\Gamma$ (see Figure \ref{fig:levelset-domain}), such that:

$\Gamma=\{ \chi \mid \phi(\chi,\tau)=0 \}$.

\noindent I define a point along the boundary curve to be:

$\chi_{\Gamma} \in \Gamma $.

\noindent With this definition of the boundary points, the level set of $\phi$ that represents the salt body boundary can be described as:

$ \phi(\chi_{\Gamma},\tau)=0 $.

\noindent While this derivation uses a 2D salt model for simplicity, a 3D salt body would instead have a four-dimensional implicit surface.


\plot{levelset-domain}{width=5in}{Diagram explaining the salt body domain ($\Omega$), the $\phi$ values inside and outside of the salt, and the salt boundary ($\Gamma$). \NR}


\subsection{Shape optimization derivation}

%=============================================
\par With a clear understanding of level sets being established, the first step of the shape optimization derivation is to define the objective function we wish to minimize. I choose a variation of the FWI objective function
\begin{equation}
    \psi(\mathbf{p}) = \frac{1}{2}|| \mathbf{F(m(p)) - d_{obs}}||^{2}_{2},
\label{eq:FWI}
\end{equation}

\noindent Here $\mathbf{F}$ is the forward acoustic wave propagator which includes a source function, wave propagation, and sampling based on a receiver geometry. Our seismic velocity model is $\mathbf{m}$, and $\mathbf{d_{obs}}$ is the observed pressure data. I deviate from the traditional FWI objective function by parameterizing $\mathbf{m}$ in terms of $\mathbf{p}$. Here I define $\mathbf{p} = [  \boldsymbol{\phi} \quad  \mathbf{b} ]^{T}$, with $\boldsymbol{\phi}$ as the implicit surface function and $\mathbf{b}$ as the background velocity function, both varying across the full spatial domain $\Theta$. In this section, I intend to take the derivative of equation \ref{eq:FWI} with respect to the underlying parameters $\mathbf{b}$ and $\boldsymbol{\phi}$. Since the chain rule applies, I first take the derivative with respect to $\mathbf{m}$ and then $\mathbf{p}$. I start by expanding our definition of the objective function:
\begin{align}
    \psi(\mathbf{p}) &= \frac{1}{2} || \mathbf{F(m(p)) - d_{obs}}||^{2}_{2}  \\
         % &= \frac{1}{2} \sum_{s=1}^{N_{s}}  ( \mathbf{F(m(p)) - d_{obs}} )_{s}^{2}  \\
         &= \frac{1}{2} ( \mathbf{F(m(p)) - d_{obs}} )^{T}( \mathbf{F(m(p)) - d_{obs}} ) \\
          &= \frac{1}{2} ( \mathbf{F(m(p))}^{T} \mathbf{F(m(p))} - 2 \mathbf{F(m(p))}^{T} \mathbf{d_{obs}} + \mathbf{d_{obs}}^{T} \mathbf{d_{obs}} ),
\label{eq:FWIexp}
\end{align}

\noindent I then take the first derivative:
\begin{align}
    \frac{d \psi}{d\mathbf{p}} &= \frac{d\mathbf{m(p)}}{d\mathbf{p}}^{T} \frac{ d \mathbf{F(m(p))}^{T}}{d\mathbf{m}}    \mathbf{F(m(p))} - \frac{d\mathbf{m(p)}}{d\mathbf{p}} ^{T} \frac{ d \mathbf{F(m(p))}^{T}}{d\mathbf{m}}   \mathbf{d_{obs}} \\
    &= \frac{d\mathbf{m(p)}}{d\mathbf{p}}^{T}  \frac{ d \mathbf{F(m(p))}^{T}}{d\mathbf{m}} ( \mathbf{F(m(p)) -  d_{obs}}) \\
    &= \frac{d\mathbf{m(p)}}{d\mathbf{p}}^{T}  \mathbf{B}^{T} ( \mathbf{F(m(p)) -  d_{obs}}).
    \label{eq:bornResult}
\end{align}

\noindent Here one recognizes $\frac{ d \mathbf{F(m(p))}^{T}}{d\mathbf{m}}$ as the familiar adjoint Born operator $(\mathbf{B}^{T})$ used in standard FWI (\cite{virieux}). However, we must continue to expand our gradient to be defined in terms of our underlying parameters $\mathbf{b}$ and $\boldsymbol{\phi}$ (contained in vector $\mathbf{p}$). This requires us to state our model space clearly:
\begin{equation}
    \mathbf{m}(\mathbf{p}) = \mathbf{m}(\boldsymbol{\phi},\mathbf{b}) = H(\boldsymbol{\phi})(\mathbf{c}_{\text{salt}} - \mathbf{b}) + \mathbf{b},
\label{eq:model}
\end{equation}


\noindent where $H(\circ)$ is the Heaviside function. Because I assume a homogeneous salt body, $\mathbf{c}_{\text{salt}}$ is a constant salt-velocity vector, typically about 4500m/s. Figure \ref{fig:model-components} explains this equation as the sum of a salt overlay created from $\boldsymbol{\phi}$ with the background velocity model $\mathbf{b}$. Note that the background velocity (shown in Figure \ref{fig:model-components}c) is defined even in the region under the salt footprint. That way if the salt shrinks to a smaller size or otherwise retracts inward, the background velocity is already defined and does not need to be interpolated.


\plot{model-components}{width=6in}{a) Full velocity model. b) Salt overlay. c) Background velocity. \NR}


\noindent I expand the definition in equation \ref{eq:model} with a Taylor series as:
\begin{equation}
    \mathbf{m_{1} = m_{0}} +  \frac { \partial \mathbf{m} }{ \partial \boldsymbol{\phi}  }  \Bigr|_{\mathbf{m_{0}}}
    \boldsymbol{\triangle \phi}  +  \frac { \partial \mathbf{m} }{ \partial \mathbf{b} }   \Bigr|_{m_{0}}
    \boldsymbol{\triangle  b} + ....
\label{eq:taylorseries}
\end{equation}

\noindent This approximation is only valid when the Taylor series converges with the addition of increasingly higher order terms. For the Heaviside function, this is not the case, since the function is not differentiable in its original form. For this reason, I use a smoothed approximation of the Heaviside function, such as:
\begin{equation} \label{eq:heaviapprox}
  \widetilde{H}(\phi) = \frac{1}{2} \left[ 1 + \frac{2}{\pi}\arctan(\frac{\pi \phi}{\epsilon}) \right] .
\end{equation}

%==============================
\noindent An advantage of using equation \ref{eq:heaviapprox} as a Heaviside approximation is that the derivative is non-zero everywhere. One downside is that the support of the function is infinite, and it will only approach +1 or 0 at $+\inf$ or $-\inf$. Further, the derivative of this function is very high near the zero crossing point. \cite{kadu2017IEEE} introduces an alternate approximation that has compact support in the region surrounding the zero-crossing:

\begin{equation} \label{eq:heaviapproxCompact}
    \widetilde{H}_{\epsilon}(\phi)=\begin{cases} 0 & \quad \text{if}\; \phi\;<\;-\epsilon, \\   \frac{1}{2}   \left[ 1+ \frac{\phi}{\epsilon} + \frac{1}{\pi}\sin{\left(\frac{\pi\phi}{\epsilon}\right)}  \right] & \quad \text{if}\; -\epsilon\; \le \;\phi\;\le\;\epsilon,  \\ 1 & \quad\text{if}\; \phi\;>\;\epsilon.   \end{cases}
\end{equation}

\noindent Besides being able to capture $+1$ and $0$ values exactly, the derivative of this approximation varies much less (see curves in Figure \ref{fig:heaviside-approx-chart}) and better balances weighting to the full boundary region when applying level set updates. By substituting this formulation of the Heaviside function in equation \ref{eq:model}, I can now truncate the series in equation \ref{eq:taylorseries} and ignore higher order terms. This creates a linear approximation for the perturbation of the velocity model $\mathbf{m}$ with respect to $\boldsymbol{\phi}$ and $\mathbf{b}$:
\begin{equation}
    \boldsymbol{\triangle m} \approx    \frac { \partial \mathbf{m}(\boldsymbol{\phi} _{ o },\mathbf{b}_{ o }) }{ \partial \boldsymbol{\phi} } \boldsymbol{\triangle \phi} +
    \frac { \partial \mathbf{m}(\boldsymbol{\phi _{ o },b_{ o }}) }{ \partial \mathbf{b} } \boldsymbol{\triangle  b} .
\end{equation}


\plot{heaviside-approx-chart}{width=5in}{Curves of Heaviside approximations based on equation \ref{eq:heaviapproxCompact} for varying values of $\epsilon$, and true Heaviside function (red). Note that within $\{-\epsilon,\epsilon\}$, the slope of each curve (its derivative) is relatively constant. \ER}



\noindent This can be written as a matrix operation:
\begin{align*}
    \boldsymbol{\triangle m}  &\approx  \begin{bmatrix}   \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \boldsymbol{\phi}}  & \frac {\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}}) }{\partial \mathbf{b}}    \end{bmatrix}
    \begin{bmatrix}  \boldsymbol{\triangle \phi} \\ \boldsymbol{\triangle b}   \end{bmatrix} \\
    \boldsymbol{\triangle m}  & \approx  \begin{bmatrix}   \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \boldsymbol{\phi}}  & \frac {\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}}) }{\partial \mathbf{b}}    \end{bmatrix} \boldsymbol{\triangle p} ,
\end{align*}


\noindent where I define operator $\mathbf{D}$ as:
\begin{align*}
    \mathbf{D} &= \begin{bmatrix}   \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \boldsymbol{\phi}}  &    \frac{\partial \mathbf{m}(\boldsymbol{\phi_{o}},\mathbf{b_{o}})}{\partial \mathbf{b}} \end{bmatrix} \\
    &= \begin{bmatrix}
    \widetilde{\delta} (\boldsymbol{\phi_{o}})(\mathbf{c}_{ \text{salt}} - \mathbf{b_{o}}) & \quad
    \mathbf{I} - \widetilde{H}_{\epsilon}(\boldsymbol{\phi_{o}})
    \end{bmatrix} .
    \stepcounter{equation}\tag{\theequation}\label{eq:Dop1}\
\end{align*}

\noindent Here, $\widetilde{H_{\epsilon}}$ is the Heaviside approximation (equation \ref{eq:heaviapproxCompact}), $\mathbf{I}$ is the identity matrix, $\widetilde{\delta}$ is the derivative of $\widetilde{H_{\epsilon}}$, $\mathbf{b}$ is the background velocity ($\mathbf{b_{0}}$ is fixed), $\boldsymbol{\phi}$ is the implicit surface ($\boldsymbol{\phi_{0}}$ is fixed), and $\mathbf{c}_{salt}$ is the constant salt velocity. Its functional form, $\widetilde{\delta}(\cdot)$ approximates an impulse function at $\phi=0$, and its application acts as a selector for the salt boundary. Because of this, the operator $\mathbf{D}$ ultimately scales and masks the parameter fields $\boldsymbol{\triangle \phi}$ and $\boldsymbol{\triangle b}$.


\par This new approximation of the perturbation can be combined in our velocity model with equation \ref{eq:bornResult} to get:

\begin{align}
    \frac{d \psi}{d\mathbf{p}} &=  \frac{d\mathbf{m(p)}}{d\mathbf{p}}^{T} \mathbf{B}^{T}( \mathbf{F(m(p)) -  d_{obs}}) \\
    &\approx  \mathbf{D}^{T} \mathbf{B}^{T}( \mathbf{F(m(p)) -  d_{obs}}) \\
    &\approx  \mathbf{D}^{T} \mathbf{B}^{T} \boldsymbol{\triangle d}.
    \label{eq:levelsetgrad}
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



% Application on simple models
\section{Simple 2D synthetic model demonstration}

\par In order to demonstrate first order (gradient) updating on $\phi$ (assuming $\mathbf{b}$ is constant for simplicity), I apply the inversion workflow shown in Algorithm \ref{alg:steepest-descent} on a 2D circular salt model example (Figure \ref{fig:initmodel-big}), with an outward normal perturbation (the initial salt is larger than the true model). This creates an error in the modeled data (Figure \ref{fig:obsdata-sample-small}) and thus a data space residual (Figure \ref{fig:residuals-sample-small}). Note that these figures show the top and bottom salt events between about 1.7-2.5 seconds.


% First order updating algorithm
\begin{algorithm}
    \caption{Steepest descent updating algorithm}
    \label{alg:steepest-descent}
    \begin{algorithmic}[1]
        \Procedure{LevelSetInversion-order1}{ $d_{\text{obs}}$,$\phi_{0}$ }

        \For {$i$ in (1,$N$)}

            \State $d_{\text{syn}}(i) = \text{F}(\phi_{i-1}) $

            \State $\text{residual}(i) = d_{\text{obs}} - d_{\text{syn}}(i) $

            \State $\text{gradient}(i) = D^{T} B^{T} \text{residual}(i) $

            \State $\alpha = \text{linesearch}(\text{gradient}(i)) $

            \State $ \phi_{i} = \phi_{i-1} - \alpha \cdot \text{gradient}(i) $

        \EndFor
        \State Return $m(\phi_{N})$
        \EndProcedure
    \end{algorithmic}
\end{algorithm}

% Obs data vs residuals
\multiplot{2}{obsdata-sample-small,residuals-sample-small}{width=0.45 \columnwidth}{ a) Observed data. b) Data residuals. Observed data and residuals are a single shot gather created from the small initial salt example (Figure \ref{fig:initmodel-small}).\ER}

\par I calculate the search direction for $\boldsymbol{\phi}$ (Figure \ref{fig:phiGrad0-big}) by back-propagating the data residual and cross-correlating it with the source wavefield as per the Born approximation imaging condition (Figure \ref{fig:rtmGrad0-big}), followed by applying the $\mathbf{D}$ operator from equation \ref{eq:Dop1}. In this case, the search direction is negative near the perturbation, since it wants to decrease what is ultimately a positive velocity error.  This search direction pushes a decrease in the value of the implicit surface (compare Figures \ref{fig:initphi-big} and \ref{fig:nextphi-big}). This decrease draws the zero-level set deeper so that it is in closer alignment with the true salt boundary. Applying the approximate Heaviside function (equation \ref{eq:heaviapproxCompact}) to this updated implicit surface (Figure \ref{fig:nextphi-big}) gives us a new model that is closer in form to the true model (Figure \ref{fig:nextmodel-big}). This process can be continued, making iterative updates to the implicit surface and as a result, the velocity model itself. 


% Big circle 2D RTMgrad (it=0)
\plot{rtmGrad0-big}{width=5in}{Search direction from adjoint Born image of back-propagated residuals used in conventional FWI. True salt extent (green dashed line), initial salt extent (black dashed line). \CR}


% Big circle 2D Phigrad (it=0)
\plot{phiGrad0-big}{width=5in}{Implicit surface search direction ($\triangle \boldsymbol{\phi}$). True salt extent (green dashed line), initial salt extent (black dashed line). \CR}

\multiplot{2}{initphi-big,nextphi-big}{width=0.85\columnwidth}{ Implicit surface ($\boldsymbol{\phi}$) model a) before, and b) after updating with search direction (Figure \ref{fig:phiGrad0-big}). True salt extent shown as green dashed line. \CR}

\multiplot{2}{initmodel-big,nextmodel-big}{width=0.85\columnwidth}{ Velocity model ($\mathbf{m}$) a) before, and b) after updating. True salt extent shown as green dashed line. \CR}

% Small circle 2D RTMgrad (it=0)
\plot{rtmGrad0-small}{width=5in}{Search direction from adjoint Born image of back-propagated residuals used in conventional FWI. True salt extent (green dashed line), initial salt extent (black dashed line). \CR}


% Small circle 2D Phigrad (it=0)
\plot{phiGrad0-small}{width=5in}{Implicit surface search direction ($\triangle \boldsymbol{\phi}$). True salt extent (green dashed line), initial salt extent (black dashed line). \CR}

\multiplot{2}{initphi-small,nextphi-small}{width=0.85\columnwidth}{ Implicit surface ($\boldsymbol{\phi}$) model a) before, and b) after updating with search direction (Figure \ref{fig:phiGrad0-small}). True salt extent shown as green dashed line. \CR}

\multiplot{2}{initmodel-small,nextmodel-small}{width=0.85\columnwidth}{ Velocity model ($\mathbf{m}$) a) before, and b) after updating. True salt extent shown as green dashed line. \CR}

\par For an initial model where the salt circle is too small (Figure \ref{fig:initmodel-small}), the adjoint Born component of the search direction (Figure \ref{fig:rtmGrad0-small}) is positive, since it is trying to increase the model velocity to that of the salt. The full search direction for $\boldsymbol{\phi}$ gives a positive perturbation of the implicit surface (Figure \ref{fig:phiGrad0-small}). This update raises the implicit surface (compare Figures \ref{fig:initphi-small} and \ref{fig:nextphi-small}), moving the salt boundary upwards to correct for the deep boundary discrepancy (Figure \ref{fig:nextmodel-small}).





































\section{Introducing the Hessian operator}

\par We can use the Newton method (equation \ref{eq:basicNewton}) to perform the inversion, which helps remove the effect of our operator from the gradient, improves the search direction, and subsequently speeds up convergence. This is done by inverting the Hessian ($\mathbf{H}$) of the objective function we are minimizing and applying it to the negative of our gradient ($\mathbf{g}$) to find the search direction $\mathbf{\triangle m}$:

\begin{equation}
    \mathbf{\triangle m = - H^{-1} g}.
    \label{eq:basicNewton}
\end{equation}


\par For our case, we can represent the Hessian as the second derivative (equation \ref{eq:fullhessianderiv}) of the FWI objective function (equation \ref{eq:FWI}):

\begin{equation} \label{eq:fullhessianderiv}
    \begin{split}
    \frac{\delta \psi}{\delta \mathbf{m}} &= \frac{d\mathbf{F(m)}}{d\mathbf{m}^{T}} (\mathbf{F(m) - d_{obs}}) \\
    \frac{\delta }{\delta \mathbf{m}} \frac{\delta \psi}{\delta \mathbf{m}} &= \frac{\delta }{\delta \mathbf{m}} \frac{d\mathbf{F(m)}}{d\mathbf{m}}^{T} (\mathbf{F(m) - d_{obs}}) \\
    \frac{\delta^{2} \psi}{\delta \mathbf{m}^{2}} &= \frac{d^{2}\mathbf{F(m)}}{d\mathbf{m}^{2}}^{T} (\mathbf{F(m) - d_{obs}}) + \frac{d\mathbf{F(m)}}{d\mathbf{m}}^{T} \frac{d\mathbf{F(m)}}{d\mathbf{m}}.
    \end{split}
\end{equation}

\noindent However, this second derivative contains a term based on the data residuals. This term can be expensive to compute and is often neglected, so the Gauss-Newton approximation is used instead:

\begin{equation} \label{eq:basichessian}
    \begin{split}
    \frac{\delta^{2} \psi}{\delta \mathbf{m}^{2}} &= \frac{d\mathbf{F(m)}}{d\mathbf{m}}^{T} \frac{d\mathbf{F(m)}}{d\mathbf{m}} + \frac{d^{2}\mathbf{F(m)}}{d\mathbf{m}^{2}}^{T} (\mathbf{F(m) - d_{obs}}) \\
    \mathbf{H_{GN}} &= \frac{d\mathbf{F(m)}}{d\mathbf{m}}^{T} \frac{d\mathbf{F(m)}}{d\mathbf{m}}
    \end{split}
\end{equation}

\par While using this approximation is cheaper to compute, it is not as accurate. \cite{fichtner} and \cite{sepHessian} show that the residual term of the FWI objective function Hessian can be found by computing WEMVA-like operations on the data-space residuals. By comparison, the Gauss-Newton approximation of the FWI objective function is simply the forward Born operator followed by the adjoint Born operator. An inherent limitation of the Born operator is that it only models first-order reflections, and so double-scattered energy such as waves that bounce inside canyons or salt bodies (Figure \ref{fig:reflection-order-diagram}) can be spatially misplaced in the search direction when using the Gauss-Newton Hessian. One would expect to gain a better search direction for canyon and salt-type models by using the full Hessian instead. For this reason, I investigate using the full Hessian on 2D synthetic models.


\plot{reflection-order-diagram}{width=5in}{Second order reflection ray paths in a canyon and inside a salt body (blue). First order reflection off water bottom (red). \NR}


\section{Hessian inversion comparisons}
As discussed in the last section, the full Hessian is potentially better than the Gauss-Newton Hessian at finding a search direction for models that contain salt canyons. I test this hypothesis by creating a synthetic salt canyon true model, and then perturb it to create a starting model so that a true search direction is known. Then I run separate inversions with the full and Gauss Newton Hessians to compare search direction results. Furthermore, I first test by perturbing only one side of the canyon, and then test by perturbing both sides. This allows us to see how the starting velocity model affects the efficacy of the Gauss-Newton and full Hessians relative to one another.

\subsection{Single canyon perturbation example}
\par For the first example, I select an upper canyon portion of the synthetic Sigsbee model (Figure \ref{fig:single-guess}). I perturb the left hand side of the canyon (Figure \ref{fig:single-pert}) to create secondary scattering against the opposite canyon wall (shown in Figure \ref{fig:centershot-analysis-single}). I use an evenly spaced acquisition geometry of 38 shots and 230 receivers, and perform modeling using absorbing boundary conditions and a Ricker wavelet with a central frequency of 15 Hz. For both the single and double perturbation cases, I assume that $\triangle \mathbf{b}=0$, and so invert for a model defined as $\triangle \mathbf{p} = \triangle \boldsymbol{\phi}$ (inverting only for the implicit surface update, not the background velocity update). 

\plot{single-guess}{width=5in}{The canyon portion of the Sigsbee model that was used.\ER}

\plot{single-pert}{width=5in}{The single canyon perturbation ($\triangle \mathbf{m_{\text{actual}}}$) of the Sigsbee model.\ER}

\plot{centershot-analysis-single}{width=5in}{Data generated on the true model ($\mathbf{d_{obs}}$) from the center shot (left). The data residual ($\triangle{\mathbf{d}}$) for the same center shot (right) generated by differencing $\mathbf{d_{obs}}$ with data generated from the single perturbation model.\CR}


\subsection{Double canyon perturbation example}
\par For the second example, I use the same true model as before (Figure \ref{fig:single-guess}). However, this time I perturb both the left and right hand sides of the canyon (Figure \ref{fig:double-pert}). This offers further complexity to the secondary scattering of the model (shown in Figure \ref{fig:centershot-analysis-double}). The same acquisition geometry and wavelet were used.

\plot{double-pert}{width=5in}{The double canyon perturbation ($\triangle{\mathbf{m_{\text{actual}}}}$) of the Sigsbee model.\ER}

\plot{centershot-analysis-double}{width=5in}{Data generated on the true model ($\mathbf{d_{obs}}$) from the center (left). The data residual ($\triangle{\mathbf{d}}$) for the same center shot (right) generated by differencing $\mathbf{d_{obs}}$ with data generated from the double perturbation model.\CR}




\subsection{Benefits of the full Hessian}

\par When comparing the results of the Gauss-Newton (Figure \ref{fig:double-final-gn}) and the full Hessian results (Figure \ref{fig:double-final-full}) from the double canyon perturbation model, one can see a slight improvement in the focusing of the energy in the full Hessian example. This improved search direction should lead to better convergence in the greater non-linear inversion scheme. When comparing against the steepest descent search direction for the double perturbation case (Figure \ref{fig:double-negative-gradient}), one can see that the search directions by either type of Hessian inversion create significant improvements. The improvement of the full Hessian versus the Gauss-Newton Hessian that is seen in the double perturbation case is minimal compared to the improvement that the Gauss-Newton Hessian has over the steepest descent direction (Figure \ref{fig:double-negative-gradient}). Similarly in the single perturbation case, the search direction from inverting the Gauss-Newton Hessian (Figure \ref{fig:single-final-gn}) is significantly better than the steepest descent search direction (Figure \ref{fig:single-negative-gradient}).


\plot{double-final-gn}{width=5in}{The Newton search direction ($\triangle \boldsymbol{\phi}$) using the inverted Gauss-Newton approximation of the Hessian on the double perturbation model.\CR}

\plot{double-final-full}{width=5in}{The Newton search direction ($\triangle \boldsymbol{\phi}$) using the inverted full Hessian on the double perturbation model.\CR}

\plot{double-negative-gradient}{width=5in}{The steepest descent search direction ($\triangle \boldsymbol{\phi}$) (negative of the FWI gradient) for the double perturbation model. \CR}

\plot{single-final-gn}{width=5in}{The Newton search direction ($\triangle{\boldsymbol{\phi}}$) using the inverted Gauss-Newton approximation of the Hessian on the single perturbation model.\CR}

\plot{single-negative-gradient}{width=5in}{The steepest descent search direction ($\triangle \boldsymbol{\phi}$) (negative of the FWI gradient) for the single perturbation model. \CR}



\subsection{Limitations}

\par However, the single perturbation case demonstrates that the advantages of the full Hessian are not realized for all models, since the Hessian is model dependent. The single perturbation example results are much different with regards to the full Hessian search direction. While the Gauss-Newton Hessian system inversion rapidly converges (Figures \ref{fig:single-final-gn} and \ref{fig:objfunc-single-gn}), the full Hessian inversion becomes unstable part way through (Figures \ref{fig:single-final-full} and \ref{fig:objfunc-single-full}). Because the full Hessian operator is not inherently positive semi-definite like the Gauss-Newton Hessian, it may have negative eigenvalues, which can lead to instability during inversion. This was very likely the case in the single canyon perturbation example. On the other hand, with the double perturbation example we get stable convergence using either method (compare Figures \ref{fig:objfunc-double-gn} and \ref{fig:objfunc-double-full}).


\plot{single-final-full}{width=5in}{The Newton search direction using the inverted full Hessian on the single perturbation model.\CR}

\multiplot{2}{objfunc-single-gn,objfunc-single-full}{width=0.75\columnwidth}{ The objective functions from the Hessian inversions using the single canyon perturbation model. Note, the values are negative because a conjugate gradient (CG) solver was used instead of a CG least-squares solver. a) Gauss-Newton Hessian. b) Full Hessian. \CR}


\multiplot{2}{objfunc-double-gn,objfunc-double-full}{width=0.75\columnwidth}{ The nearly identical objective functions from the Hessian inversions using the double canyon perturbation model. Note, the values are negative because a conjugate gradient (CG) solver was used instead of a CG least-squares solver. a) Gauss-Newton Hessian. b) Full Hessian. \CR}



\par One of the few feasible methods for enforcing our operator to be positive semi-definite is to use the Levenberg-\cite{Levenberg} method of regularizing the operator with a scaled identity matrix: 

\begin{equation} \label{eq:levenberg}
    \mathbf{\hat{H}_{full} = H_{full} + \alpha I}
\end{equation}

\noindent However, in order to use this method properly, the correct scaling of the identity matrix must be found. If too large of a scaling is selected, the operator becomes more like a scaled identity matrix, negating the potential benefit of inverting the full Hessian system to begin with. If the scaling is too small, the system will still be ill-conditioned, and prone to instability as observed earlier. The ideal scaling is slightly more than the value of the most negative eigenvalue of the operator. This makes the operator positive definite. Since our model (and as a result, our Hessian) is very large, it is impractical to store or factorize the Hessian matrix to determine the most negative eigenvalue through traditional non-iterative linear algebra methods. 


\plot{powerit1}{width=6in}{The power iteration curve showing the progressing approximation of the maximum absolute value eigenvalue of the full Hessian operator used on the single canyon perturbation model. \CR}

\subsubsection{Power Iteration Method}
\par The most practical way to find the best scaling is by using the power iteration method outlined in \cite{larson2009elementary} to find the maximum absolute-valued eigenvalue (positive in the case shown for Figure \ref{fig:powerit1}). After this has been found, one shifts the diagonal of the operator by the negative of this value, and then repeat the power iterations to find a new maximum absolute-valued eigenvalue. The difference between this value and the first one derived is the magnitude of the most negative eigenvalue. I experimented with this method, but found the benefits of this effort to be minimal, and at notable computational cost. Figure \ref{fig:powerit1} shows that in practice at least 25 iterations (and so $\sim25$ forward full Hessian operator applications) were necessary for each of the two power iteration searches. Once these searches were complete and a proper shift was found, I found that the results of using this Levenberg-Marquardt shift were almost imperceptible from the Gauss-Newton results. Furthermore, since the Hessian operator is model-dependent (and so changes with each outer loop iteration of FWI), these power iteration steps would need to be performed each time the Newton system was inverted. 


\section{Conclusions} 
\par Level set concepts can be combined with the FWI objective function to create a shape optimization scheme that allows an elegant way to address the problem of finding an optimal salt body boundary. My demonstrations on simple 2D examples illustrate the relationship between the FWI and implicit surface search directions, and the effect that updating this new model space ultimately has on the velocity model. When I investigate the use of the inverse Hessian to refine the search direction, I find that the Gauss-Newton Hessian approximation is sufficient to improve convergence. However, the theoretically more accurate full Hessian gives mixed results which depend on the model. Robust inversion can only be assured by performing more computation (power iterations) to find an optimal correction for the diagonal elements of the operator. For this reason, I find that the impracticalities of maintaining stability in the full Hessian inversion outweigh the potential benefits from using it. While the Gauss-Newton Hessian is less accurate than the full Hessian, its inversion is stable, and at far less cost.







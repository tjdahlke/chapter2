module scats1_mod

  implicit none

  integer, private                         :: n1, n2, nt, it, ix, iz
  real, dimension(:,:,:), pointer, private :: wflds1

  contains

  subroutine scats1_init(n1_in, n2_in, nt_in, wflds1_in)
    integer                        :: n1_in, n2_in, nt_in
    real, dimension(:,:,:), target :: wflds1_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    wflds1 => wflds1_in
  end subroutine

  function scats1_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call scats1_op2(adj, add, model, data)
    stat=0
  end function

  subroutine scats1_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2)      :: model
    real, dimension(n1,n2,nt) :: data
    if(adj) then
      if(.not. add) model = 0.
      do it=1,nt
        model = model + data(:,:,it)*wflds1(:,:,it)
      end do
    else
      if(.not. add) data = 0.
      do it=1,nt
        data(:,:,it) = data(:,:,it) + model*wflds1(:,:,it)
      end do
    end if
  end subroutine

end module

module comb_mod

  use laplace_mod
  use bcder_mod

  implicit none

  integer, private                           :: n1, n2
  real, dimension(:,:), pointer, private     :: veldt2, vwt, wt
  real, dimension(:,:), allocatable, private :: temp

  contains

  subroutine comb_init(n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in, d1_in, d2_in, veldt2_in, vwt_in, wt_in)
    integer                      :: n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in
    real                         :: d1_in, d2_in
    real, dimension(:,:), target :: veldt2_in, vwt_in, wt_in
    n1 = n1_in
    n2 = n2_in
    veldt2 => veldt2_in
    vwt => vwt_in
    wt => wt_in
    if(.not. allocated(temp)) allocate(temp(n1,n2))
    call laplace_init(n1, n2, d1_in, d2_in)
    call bcder_init(n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in)
  end subroutine

  function comb_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call comb_op2(adj, add, model, data)
    stat=0
  end function

  subroutine comb_op2(adj, add, model, data)
    logical,intent(in)       :: adj, add 
    real, dimension(n1,n2) :: model, data
    if(adj) then
      if(.not. add) model = 0.
      temp = veldt2*(1.-wt)*data
      call laplace_op2(adj, .true., model, temp)
      model = model + (2.-wt)*data
      temp = -vwt*wt*data
      call bcder_op2(adj, .true., model, temp)
    else
      if(.not. add) data = 0.
      call laplace_op2(adj, .false., model, temp)
      data = data + (1.-wt)*(veldt2*temp + 2.*model)
      call bcder_op2(adj, .false., model, temp)
      data = data + wt*(model - vwt*temp)
    end if
  end subroutine

end module

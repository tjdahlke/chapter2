
!===============================================================================
!
!
!		MODEL:
!		DATA:
!===============================================================================

PROGRAM RBF_BUILD_TABLE
  USE sep
  USE rbf_mod
  IMPLICIT NONE

  INTEGER 														:: nrbf, nz, nx, trad
  REAL 																:: beta, threshold, timer_sum=0.
  REAL, DIMENSION(:,:), ALLOCATABLE		:: rbftable
  LOGICAL 														:: verbose
  INTEGER, DIMENSION(8) 							:: timer0, timer1

  CALL sep_init()
  CALL DATE_AND_TIME(values=timer0)

  IF(verbose) WRITE(0,*) "================ Read in initial parameters /allocate =================="
  CALL from_param("n1", nz)
  CALL from_param("n2", nx)
  CALL from_param("threshold", threshold, 0.01)
  CALL from_param("verbose", verbose, .TRUE.)
  CALL from_param("trad", trad, 15)
  CALL from_param("beta", beta, 0.15)
  ALLOCATE(rbftable(2*trad+1, 2*trad+1))

  !##########################################################################################
  IF(verbose) WRITE(0,*) "========= Initialize the submodules ====================================="
  CALL rbf_init_buildtable(trad,threshold,beta)
  CALL rbftable_build(rbftable)
  !##########################################################################################

  IF(verbose) WRITE(0,*) "========= Write out RBF table ================="
  CALL to_history("n1",2*trad+1)
  CALL to_history("n2",2*trad+1)
  CALL to_history("label1","x")
  CALL to_history("label2","z")
  CALL sep_write(rbftable)

  !-----------------------------------------------------------------------------
  CALL DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  WRITE(0,*) "timer sum (sec): ", timer_sum/1000
  IF (verbose) WRITE(0,*) "RBF_BUIL_TABLE program complete"
  DEALLOCATE(rbftable)
END PROGRAM RBF_BUILD_TABLE

!==============================================================
!	RADIAL BASIS FUNCTION MODULE 2D
!
!	7/19/2018,taylor@sep.stanford.edu
! 
! This code needs to be compiled AND linked with -qopenmp!!
! Otherwise it wont run correctly!!
!==============================================================


module rbf_mod2d
	use sep
	use mem_mod
	implicit none
	real,private								:: beta,eps,threshold
	integer,private								:: nz,ny,nrbf,trad
	real,allocatable,dimension(:,:),private		:: temp1,temp3,m0,Hterm,phirbf
	REAL,PARAMETER 								:: Pi = 3.1415927
	real,dimension(:,:),pointer,private 		:: rbfcoord
	real,dimension(:,:),pointer,private 		:: masksalt,rbftable
	contains
	!==============================================================


	subroutine rbf_init(nz_in,ny_in,nrbf_in,trad_in,rbfcoord_in,rbftable_in)
		integer							:: nz_in,ny_in,nrbf_in,trad_in
		real,dimension(:,:),target		:: rbfcoord_in
		real,dimension(:,:),target		:: rbftable_in
		rbfcoord => rbfcoord_in
		rbftable => rbftable_in
		nrbf = nrbf_in
		nz = nz_in
		ny = ny_in
		trad = trad_in
		allocate(temp1(nz,ny))
		allocate(temp3(nz,ny))
		call sep_init()
	end subroutine


	subroutine rbf_init_buildtable(trad_in,threshold_in,beta_in)
		integer				:: trad_in
		real				:: threshold_in,beta_in
		trad = trad_in
		threshold = threshold_in
		beta = beta_in
	end subroutine


	subroutine rbf_init_mask(masksalt_in)
		real,dimension(:,:),target	:: masksalt_in
		masksalt => masksalt_in
	end subroutine



	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Build the RBF gaussian curve template ---------------------
	subroutine rbftable_build(rbftable)
		real,dimension(:,:)	:: rbftable
		real					:: radius,yy,zz
		integer					:: iz,iy
		!!!race_cond_present!$omp parallel do
		do iy=1,(2*trad+1)
			yy = (-trad+iy-1)**2 
			do iz=1,(2*trad+1)
				zz = (-trad+iz-1)**2
				radius = SQRT((zz + yy))
				rbftable(iz,iy) = EXP(-((beta*radius)**2))
			end do
		end do
		!!!race_cond_present!$omp end parallel do
	end subroutine





	subroutine rbf_kernel(cz,cy,left1,left2,right1,right2,top1,top2,bot1,bot2)
	    implicit none
		integer,intent(in)	:: cz,cy
		integer,intent(out)	:: left1,left2,right1,right2
		integer,intent(out)	:: top1,top2,bot1,bot2

		left1 = MAXVAL((/1,cy-trad/))
		right1 = MINVAL((/ny,cy+trad/))
		top1 = MAXVAL((/1,cz-trad/))
		bot1 = MINVAL((/nz,cz+trad/))

		left2 = MAXVAL((/1,trad+2-cy/))
		right2 = MINVAL((/2*trad+1,ny-cy+trad+1/))
		top2 = MAXVAL((/1,trad+2-cz/))
		bot2 = MINVAL((/2*trad+1,nz-cz+trad+1/))

	end subroutine






	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Make full RBF (apply weights to kernels) --------------------------------
	subroutine rbf(adj,add,model,data)
		real,dimension(:)		:: model
		real,dimension(:,:)		:: data
		integer					:: cz,cy,izz,iyy,ir,qq
		logical					:: adj,add
		integer					:: left1,left2,right1,right2
		integer					:: top1,top2,bot1,bot2	
		! integer 				:: NTHREADS, TID, OMP_GET_NUM_THREADS, OMP_GET_THREAD_NUM


		temp1=0.0
		if (.not. add) then
			if (adj) then
				model=0.0
			else
				data=0.0
			endif
		endif


		if (adj) then
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cy = rbfcoord(ir,2)

				call rbf_kernel(cz,cy,left1,left2,right1,right2,top1,top2,bot1,bot2)
				temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

				do iyy=left1,right1
					do izz=top1,bot1
						model(ir) = model(ir) + data(izz,iyy)*temp1(izz,iyy)
					end do
				end do

			end do
		else
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cy = rbfcoord(ir,2)

				call rbf_kernel(cz,cy,left1,left2,right1,right2,top1,top2,bot1,bot2)
				temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

				do iyy=left1,right1
					do izz=top1,bot1
						data(izz,iyy) = data(izz,iyy) + model(ir)*temp1(izz,iyy)
					end do
				end do

			end do
		end if


	end subroutine





	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Apply Linearized derivative of Heaviside of RBF  ------------------------
	subroutine L_Hrbf(adj,add,model,data)
		real,dimension(:)		:: model
		real,dimension(:,:)		:: data
		integer					:: cz,cy,izz,iyy,ir
		logical					:: adj,add
		integer,dimension(8)	:: timer0,timer1,timer2,val
		real 					:: timer_sum1=0.,timer_sum2=0.
		integer					:: left1,left2,right1,right2
		integer					:: top1,top2,bot1,bot2

		temp1=0.0
		if (.not. add) then
			if (adj) then
				model=0.0
			else
				data=0.0
			endif
		endif

! Possible to loop over the sparse mask space and make quicker?

		if (adj) then
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cy = rbfcoord(ir,2)

				call rbf_kernel(cz,cy,left1,left2,right1,right2,top1,top2,bot1,bot2)
				temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

				do iyy=left1,right1
					do izz=top1,bot1
						model(ir) = model(ir) + data(izz,iyy)*temp1(izz,iyy)*masksalt(izz,iyy)
					end do
				end do

			end do
		else
			do ir=1,nrbf
				cz = rbfcoord(ir,1)
				cy = rbfcoord(ir,2)

				call rbf_kernel(cz,cy,left1,left2,right1,right2,top1,top2,bot1,bot2)
				temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

				do iyy=left1,right1
					do izz=top1,bot1
						data(izz,iyy) = data(izz,iyy) + model(ir)*temp1(izz,iyy)*masksalt(izz,iyy)
					end do
				end do

			end do
		end if

	end subroutine





	!/////////////////////////////////////////////////////////////////////////////////////
	!----------- Apply Heaviside to RBF (Non-linear forward) -----------------------------
	subroutine Hrbf(model,data)
		real,dimension(:)		:: model
		real,dimension(:,:)		:: data
		integer					:: izz, iyy
		call rbf(.False.,.False.,model,temp3)

		data=0.0
		! For case where nothing is salt (start of NL inversion). 
		! Otherwise there is no salt "seed" for the linearization step
		if (sum(temp3)==0.0) then
			write(0,*) "~~~~~~~~~~~~~  Using >=0.0  ~~~~~~~~~~~~~~~~"
			data = 1.0
		else
			write(0,*) "~~~~~~~~~~~~~  Using >0.0  ~~~~~~~~~~~~~~~~"
			data=0.0
			!!!race_cond_present!$omp parallel do
			do iyy=1,ny
				! write(0,*) "percent done = ", 100.0*iyy/ny
				do izz=1,nz
					if (temp3(izz,iyy)>0.0) data(izz,iyy) = 1.0
				end do
			end do
			!!!race_cond_present!$omp end parallel do	
		endif

		if (exist_file('test')) then
			call to_history("n1",nz,'test')
			call to_history("o1",0,'test')
			call to_history("d1",1,'test')
			call to_history("n2",ny,'test')
			call to_history("o2",0,'test')
			call to_history("d2",1,'test')
			call sep_write(temp3,'test')
		endif

	end subroutine



end module
!===============================================================================
!		Linearized Derivative of the Heaviside applied to a Radial Basis function
!
!		d/dz(H(RBF(z)))
!
!		MODEL:  Nrbf
!		DATA:  	Nz x Ny
!===============================================================================

program LINEARIZED_RBF2d
	use sep
	use rbf_mod2d
	use gradient_mod

	implicit none
	integer								:: nrbf,nz,ny,trad,iy,iz
	real								:: timer_sum=0.
	real,dimension(:,:),allocatable		:: data,bin,binZ,binY,masksalt,rbftable
	real,dimension(:,:),allocatable		:: rbfcoord
	real,dimension(:),allocatable		:: model
	logical								:: verbose,adjoint
	integer,dimension(8)				:: timer0,timer1

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	if(verbose) write(0,*) "================ Read in initial parameters =================="
	call from_param("n1r",nz)
	call from_param("n2r",ny)
	call from_param("verbose",verbose,.TRUE.)
	call from_param("adjoint",adjoint,.FALSE.)
	call from_param("trad",trad,15)
	call from_aux("rbfcoord","n1",nrbf)

	allocate(rbftable((2*trad+1),(2*trad+1)))
	allocate(masksalt(nz,ny))
	allocate(model(nrbf))
	allocate(data(nz,ny))
	allocate(bin(nz,ny))
	allocate(binZ(nz,ny))
	allocate(binY(nz,ny))
	allocate(rbfcoord(nrbf,3))

	call sep_read(rbfcoord,"rbfcoord")
	call sep_read(rbftable,"rbftable")

	if (adjoint) then
		if (verbose) write(0,*) "================ Reading in RBF surface perturbation ================"
		call sep_read(data)
		data=2*data ! To create a implicit surface that is +1 in salt and -1 outside salt
		bin=data
	else
		if (verbose) write(0,*) "==================== Reading in RBF weights perturbation ================="
		call sep_read(model)
		model=2*model ! To create a implicit surface that is +1 in salt and -1 outside salt
		call Hrbf(model,bin)
	endif


!##########################################################################################
!	MAKE GRADIENT
	! dz gradient
	call gradient_init(nz,1.0)
	do iy=1,ny
		call gradient_op(.false.,.false.,bin(:,iy),binZ(:,iy))
	enddo
	! dy gradient
	call gradient_init(ny,1.0)
	do iz=1,nz
		call gradient_op(.false.,.false.,bin(iz,:),binY(iz,:))
	enddo
	masksalt = sqrt(binZ**2 + binY**2)
	WHERE (masksalt >0.0) masksalt = 1.0
!##########################################################################################
	
	if (exist_file('masksaltout')) then
		call to_history("n1"    ,nz	,'masksaltout')
		call to_history("n2"    ,ny	,'masksaltout')
		call to_history("label1",'z','masksaltout')
		call to_history("label2",'y','masksaltout')
		call sep_write(masksalt,"masksaltout")
	end if

!##########################################################################################
	if(verbose) write(0,*) "========= Initialize the submodules ====================================="
	call rbf_init(nz,ny,nrbf,trad,rbfcoord,rbftable)
	call rbf_init_mask(masksalt)
	call L_Hrbf(adjoint,.False.,model,data)

!##########################################################################################

	if (adjoint) then
		if(verbose) write(0,*) "========= Write out RBF file (weights perturbation) ================="
		call to_history("n1",nrbf)
		call to_history("n2",1)
		call to_history("n3",1)
		call to_history("o2",0.0)
		call to_history("o3",0.0)
		call to_history("d2",1.0)
		call to_history("d3",1.0)
		call to_history("label1",'# sparse rbf centers')
		call sep_write(model)
	else
		if(verbose) write(0,*) "========= Write out RBF file (surface perturbation) ================="
		call to_history("n1",nz)
		call to_history("n2",ny)
		call to_history("label1",'z [km]')
		call to_history("label2",'y [km]')
		call sep_write(data)
	end if

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec): ",timer_sum/1000
	deallocate(data,model)
	if (verbose) write(0,*) "LINEARIZED_RBF program complete"

end program
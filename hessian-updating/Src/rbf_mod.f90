!==============================================================
!	RADIAL BASIS FUNCTION MODULE 2D
!
!	7/7/2017,taylor@sep.stanford.edu
!==============================================================


MODULE rbf_mod
  USE sep
  USE mem_mod
  IMPLICIT NONE
  REAL,PRIVATE															:: beta,eps,threshold
  INTEGER,PRIVATE														:: nz,nx,nrbf,trad
  REAL,ALLOCATABLE,DIMENSION(:,:),PRIVATE		:: temp1,temp3,m0,Hterm,phirbf
  REAL,PARAMETER 														:: Pi = 3.1415927
  REAL,DIMENSION(:,:),POINTER,PRIVATE 			:: rbfcoord
  REAL,DIMENSION(:,:),POINTER,PRIVATE 			:: masksalt,rbftable
CONTAINS
  !==============================================================


  SUBROUTINE rbf_init(nz_in,nx_in,nrbf_in,trad_in,rbfcoord_in,rbftable_in)
    INTEGER												:: nz_in,nx_in,nrbf_in,trad_in
    REAL,DIMENSION(:,:),TARGET		:: rbfcoord_in
    REAL,DIMENSION(:,:),TARGET		:: rbftable_in
    rbfcoord => rbfcoord_in
    rbftable => rbftable_in
    nrbf = nrbf_in
    nz = nz_in
    nx = nx_in
    trad = trad_in
    ALLOCATE(temp1(nz,nx))
    ALLOCATE(temp3(nz,nx))
    CALL sep_init()
  END SUBROUTINE rbf_init


  SUBROUTINE rbf_init_buildtable(trad_in,threshold_in,beta_in)
    INTEGER			:: trad_in
    REAL				:: threshold_in,beta_in
    trad = trad_in
    threshold = threshold_in
    beta = beta_in
  END SUBROUTINE rbf_init_buildtable


  SUBROUTINE rbf_init_mask(masksalt_in)
    REAL,DIMENSION(:,:),TARGET	:: masksalt_in
    masksalt => masksalt_in
  END SUBROUTINE rbf_init_mask



  !/////////////////////////////////////////////////////////////////////////////////////
  !----------- Build the RBF gaussian curve template ---------------------
  SUBROUTINE rbftable_build(rbftable)
    REAL,DIMENSION(:,:)	:: rbftable
    REAL 								:: radius,xx,zz
    INTEGER 						:: ix,iz
		!$OMP DO
    DO ix=1,(2*trad+1)
       xx = (-trad+ix-1)**2
       DO iz=1,(2*trad+1)
          zz = (-trad+iz-1)**2
          radius = SQRT((zz + xx))
          rbftable(iz,ix) = EXP(-((beta*radius)**2))
       END DO
    END DO
		!$OMP END DO
  END SUBROUTINE rbftable_build





  SUBROUTINE rbf_kernel(cz,cx,left1,left2,right1,right2,top1,top2,bot1,bot2)
    IMPLICIT NONE
    INTEGER,INTENT(in)	:: cz,cx
    INTEGER,INTENT(out)	:: left1,left2,right1,right2
    INTEGER,INTENT(out)	:: top1,top2,bot1,bot2

    left1 = MAXVAL((/1,cx-trad/))
    right1 = MINVAL((/nx,cx+trad/))
    top1 = MAXVAL((/1,cz-trad/))
    bot1 = MINVAL((/nz,cz+trad/))

    left2 = MAXVAL((/1,trad+2-cx/))
    right2 = MINVAL((/2*trad+1,nx-cx+trad+1/))
    top2 = MAXVAL((/1,trad+2-cz/))
    bot2 = MINVAL((/2*trad+1,nz-cz+trad+1/))

  END SUBROUTINE rbf_kernel






  !/////////////////////////////////////////////////////////////////////////////////////
  !----------- Make full RBF (apply weights to kernels) --------------------------------
  SUBROUTINE rbf(adj,add,model,DATA)
    REAL,DIMENSION(:)		:: model
    REAL,DIMENSION(:,:)	:: DATA
    INTEGER							:: cz,cx,ixx,izz,ir
    LOGICAL							:: adj,add
    INTEGER							:: left1,left2,right1,right2
    INTEGER							:: top1,top2,bot1,bot2

    temp1=0.0
    IF (.NOT. add) THEN
       IF (adj) THEN
          model=0.0
       ELSE
          DATA=0.0
       ENDIF
    ENDIF


    DO ir=1,nrbf
       cz = rbfcoord(ir,1)
       cx = rbfcoord(ir,2)

       CALL rbf_kernel(cz,cx,left1,left2,right1,right2,top1,top2,bot1,bot2)
       temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

       IF (adj) THEN
					!$OMP DO
          DO ixx=left1,right1
             DO izz=top1,bot1
                model(ir) = model(ir) + DATA(izz,ixx)*temp1(izz,ixx)
             END DO
          END DO
					!$OMP END DO
       ELSE
				 !$OMP DO
          DO ixx=left1,right1
             DO izz=top1,bot1
                DATA(izz,ixx) = DATA(izz,ixx) + model(ir)*temp1(izz,ixx)
             END DO
          END DO
					!$OMP END DO
       ENDIF
    END DO

  END SUBROUTINE rbf





  !/////////////////////////////////////////////////////////////////////////////////////
  !----------- Apply Linearized derivative of Heaviside of RBF  ------------------------
  SUBROUTINE L_Hrbf(adj,add,model,DATA)
		REAL,DIMENSION(:)			:: model
		REAL,DIMENSION(:,:)		:: DATA
    INTEGER								:: cz,cx,ixx,izz,ir,thread_num,omp_get_max_threads
    LOGICAL								:: adj,add
    INTEGER,DIMENSION(8)	:: timer0,timer1,timer2,val
    REAL 									:: timer_sum1=0.,timer_sum2=0.
    INTEGER								:: left1,left2,right1,right2
    INTEGER								:: top1,top2,bot1,bot2

    temp1=0.0
    IF (.NOT. add) THEN
       IF (adj) THEN
          model=0.0
       ELSE
          DATA=0.0
       ENDIF
    ENDIF

    DO ir=1,nrbf
        ! write(0,*) "percent done",100*ir/nrbf
        cz = rbfcoord(ir,1)
        cx = rbfcoord(ir,2)

        CALL rbf_kernel(cz,cx,left1,left2,right1,right2,top1,top2,bot1,bot2)
        temp1(top1:bot1,left1:right1) = rbftable(top2:bot2,left2:right2)

        ! thread_num = omp_get_max_threads( )
        ! write(0,*) "The number of threads available    =" ,thread_num

       IF (adj) THEN
          !!$omp parallel shared (ny,nc,nr,interior, left, right, top, bottom,in,out) private ( i, j, k,ul,bl,ur,br)
          !!$omp do
          DO ixx=left1,right1
             DO izz=top1,bot1
                model(ir) = model(ir) + DATA(izz,ixx)*temp1(izz,ixx)*masksalt(izz,ixx)
             END DO
          END DO
					!!$OMP END DO
       ELSE
					!!$OMP DO
          DO ixx=left1,right1
             DO izz=top1,bot1
                DATA(izz,ixx) = DATA(izz,ixx) + model(ir)*temp1(izz,ixx)*masksalt(izz,ixx)
             END DO
          END DO
					!!$OMP END DO
       ENDIF

    END DO

  END SUBROUTINE L_Hrbf





  !/////////////////////////////////////////////////////////////////////////////////////
  !----------- Apply Heaviside to RBF (Non-linear forward) -----------------------------
  SUBROUTINE Hrbf(model,DATA)
    REAL,DIMENSION(:)		:: model
    REAL,DIMENSION(:,:)	:: DATA
    INTEGER					:: izz, ixx
    CALL rbf(.FALSE.,.FALSE.,model,temp3)

    DATA=0.0
    ! For case where nothing is salt (start of NL inversion).
    ! Otherwise there is no salt "seed" for the linearization step
    IF (SUM(temp3)==0.0) THEN
       WRITE(0,*) "~~~~~~~~~~~~~  Using >=0.0  ~~~~~~~~~~~~~~~~"
       DATA = 1.0
    ELSE
       WRITE(0,*) "~~~~~~~~~~~~~  Using >0.0  ~~~~~~~~~~~~~~~~"
       DATA=0.0
			 !$OMP DO
       DO ixx=1,nx
          DO izz=1,nz
             IF (temp3(izz,ixx)>0.0) DATA(izz,ixx) = 1.0
          END DO
       END DO
			 !$OMP END DO
    ENDIF

    IF (exist_file('test')) THEN
       CALL to_history("n1",nz,'test')
       CALL to_history("o1",0,'test')
       CALL to_history("d1",1,'test')
       CALL to_history("n2",nx,'test')
       CALL to_history("o2",0,'test')
       CALL to_history("d2",1,'test')
       CALL to_history("n3",1,'test')
       CALL to_history("o3",0,'test')
       CALL to_history("d3",1,'test')
       CALL sep_write(temp3,'test')
    ENDIF

  END SUBROUTINE Hrbf



END MODULE rbf_mod

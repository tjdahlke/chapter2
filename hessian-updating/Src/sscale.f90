module sscale_mod

  implicit none

  integer, private                       :: n1, n2, nt, it
  real, private                          :: dtw
  real, dimension(:,:), pointer, private :: veldt2, wt

  contains

  subroutine sscale_init(n1_in, n2_in, nt_in, dtw_in, veldt2_in, wt_in)
    integer                      :: n1_in, n2_in, nt_in
    real                         :: dtw_in
    real, dimension(:,:), target :: veldt2_in, wt_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    dtw = dtw_in
    veldt2 => veldt2_in
    wt => wt_in
  end subroutine

  function sscale_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call sscale_op2(adj, add, model, data)
    stat=0
  end function

  subroutine sscale_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2,nt) :: model, data
    if(adj) then
      if(.not. add) model = 0.
      !$omp parallel do
      do it=1,nt
        model(:,:,it) = model(:,:,it) + (wt-1.)*veldt2*data(:,:,it)
      end do
      !$omp end parallel do
    else
      if(.not. add) data = 0.
      !$omp parallel do
      do it=1,nt
        data(:,:,it) = data(:,:,it) + (wt-1.)*veldt2*model(:,:,it)
      end do
      !$omp end parallel do
    end if
  end subroutine

end module

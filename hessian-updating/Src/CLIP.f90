
!===============================================================================
!		Applies the clipping
!===============================================================================

program CLIP
use sep
implicit none

	integer																:: n1, n2
	logical																:: verbose
	real																	:: clipval
	real, dimension(:,:), allocatable			:: input

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("clipval",clipval,1.5)

	call from_history("n1",n1)
	call from_history("n2",n2)


	if(verbose) write(0,*) "========= Allocate ==================================="
	allocate(input(n1, n2))
	call sep_read(input)
	WHERE (input <=clipval) input = clipval
	call sep_write(input)

end program

module step_mod

  use comb_mod
  use mem_mod

  implicit none

  integer, private                           :: n1, n2, nt2, it2
  real, dimension(:,:), pointer, private     :: wt
  real, dimension(:,:), allocatable, private :: temp

  contains

  subroutine step_init(n1_in, n2_in, nt2_in, bc1_in, bc2_in, bc3_in, bc4_in, d1_in, d2_in, veldt2_in, vwt_in, wt_in)
    integer                        :: n1_in, n2_in, nt2_in, bc1_in, bc2_in, bc3_in, bc4_in
    real                           :: d1_in, d2_in
    real, dimension(:,:), target   :: veldt2_in, vwt_in, wt_in
    n1 = n1_in
    n2 = n2_in
    nt2 = nt2_in
    wt => wt_in
    call mem_alloc2d(temp, n1, n2)
    call comb_init(n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in, d1_in, d2_in, veldt2_in, vwt_in, wt_in)
  end subroutine

  function step_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call step_op2(adj, add, model, data)
    stat=0
  end function

  subroutine step_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2,nt2) :: model
    real, dimension(n1,n2,nt2) :: data
    if(adj) then
      if(.not. add) model = 0.
      model(:,:,nt2) = model(:,:,nt2) + data(:,:,nt2)
      call comb_op2(adj, .false., temp, model(:,:,nt2))
      model(:,:,nt2-1) = model(:,:,nt2-1) + data(:,:,nt2-1) + temp
      do it2=nt2-2,1,-1
        call comb_op2(adj, .false., temp, model(:,:,it2+1))
        model(:,:,it2) = model(:,:,it2) + data(:,:,it2) + temp + (wt-1.)*model(:,:,it2+2)
      end do
    else
      if(.not. add) data = 0.
      data(:,:,1) = data(:,:,1) + model(:,:,1)
      call comb_op2(adj, .false., data(:,:,1), temp)
      data(:,:,2) = data(:,:,2) + model(:,:,2) + temp
      do it2=3,nt2
        call comb_op2(adj, .false., data(:,:,it2-1), temp)
        data(:,:,it2) = data(:,:,it2) + model(:,:,it2) + temp + (wt-1.)*data(:,:,it2-2)
      end do
    end if
  end subroutine

end module

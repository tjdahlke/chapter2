
!===================================================================================
!		Assemble the velocity model from the implicit surface and background vel
!===================================================================================

program MAKE_VEL_MODEL
use sep
implicit none

	integer										:: nz, nx, ny
	logical										:: verbose
	real										:: vsalt
	real, dimension(:,:), allocatable			:: Heavi2d, velback2d, saltvel2d, final2d
	real, dimension(:,:,:), allocatable			:: Heavi3d, velback3d, saltvel3d, final3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("vsalt",vsalt,4500.0)
	call from_aux("heaviside", "n1", nz)
	call from_aux("heaviside", "n2", nx)
	call from_aux("heaviside", "n3", ny,1)

	if (ny==1) then
		if(verbose) write(0,*) "========= Detected 2D inputs ==============="
		allocate(Heavi2d(nz, nx))
		allocate(saltvel2d(nz, nx))
		allocate(velback2d(nz, nx))
		allocate(final2d(nz, nx))
		call sep_read(velback2d)
		call sep_read(Heavi2d,"heaviside")
		saltvel2d=vsalt

		final2d=(saltvel2d-velback2d)*Heavi2d + velback2d
		call sep_write(final2d)
	else
		if(verbose) write(0,*) "========= Detected 3D inputs ==============="
		allocate(Heavi3d(nz, nx, ny))
		allocate(saltvel3d(nz, nx, ny))
		allocate(velback3d(nz, nx, ny))
		allocate(final3d(nz, nx, ny))
		call sep_read(velback3d)
		call sep_read(Heavi3d,"heaviside")
		saltvel3d=vsalt

		final3d=(saltvel3d-velback3d)*Heavi3d + velback3d
		call sep_write(final3d)
	end if

end program










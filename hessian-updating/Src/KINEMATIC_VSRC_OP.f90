program KINEMATIC_VSRC_OP

  ! Program for calculating the kinematic virtual source as described by Xukai Shen in his thesis from 2015.
  !
  ! Inputs:
  !   1) history file of input data (same size as syntraces); standard input
  !   2) history file of syntraces

  ! Outputs:
  !   1) history file of virtual source data; standard output
  !
  ! Taylor Dahlke, October 2018
  !
  !VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV


  !=================================
  ! Standard declarations
  !=================================
  use sep
  implicit none


  !=================================
  ! Declare variables
  !=================================
  ! Program variables
  integer                           :: ii,ntraces,nt
  real                              :: synnorm,ot
  real, dimension(:),allocatable    :: syntraceO,syntraceN,model,data
  logical                           :: adjoint
  call sep_init()

  ! History file parameters
  call from_history('n1',nt)
  call from_history('n2',ntraces)

  call from_aux("syntraces","o1",ot)
  call from_param("adjoint",adjoint,.False.)
  call to_history('o1',ot)

  ! Make arrays
  allocate(syntraceO(nt))
  allocate(syntraceN(nt))
  allocate(model(nt))
  allocate(data(nt))


  ! Make virtual source traces
  if (adjoint) then
      do ii=0,ntraces

          ! Read the traces
          model=0.0
          write(0,*) "percent done: ", 100*ii/ntraces
          call sep_read(syntraceO,"syntraces")
          call sep_read(data)

          ! Find trace norms
          synnorm=SQRT(DOT_PRODUCT(syntraceO,syntraceO))

          if ((synnorm**3)/=0.0) then
              ! Only normalize when norms are non-zero
              syntraceN = syntraceO/synnorm
          else
              data=0.0
              call sep_write(model)
              ! Dont bother doing the rest in this case
              cycle
          end if

          ! Calculate the virtual source
          model = model - (DOT_PRODUCT(data,syntraceO)/(synnorm**3))*syntraceO
          model = model + (data/synnorm) 

          ! Write the output trace
          call sep_write(model)

      end do
  else
      do ii=0,ntraces

          ! Read the traces
          data=0.0
          write(0,*) "percent done: ", 100*ii/ntraces
          call sep_read(syntraceO,"syntraces")
          call sep_read(model)

          ! Find trace norms
          synnorm=SQRT(DOT_PRODUCT(syntraceO,syntraceO))

          if ((synnorm**3)/=0.0) then
              ! Only normalize when norms are non-zero
              syntraceN = syntraceO/synnorm
          else
              data=0.0
              call sep_write(data)
              ! Dont bother doing the rest in this case
              cycle
          end if

          ! Calculate the virtual source
          data = data + (model/synnorm) 
          data = data - (DOT_PRODUCT(model,syntraceO)/(synnorm**3))*syntraceO

          ! Write the output trace
          call sep_write(data)

      end do
  end if



  write(0,*) " DONE!"

end program





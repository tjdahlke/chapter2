!###############################################################################
!
!	Calculates the gradient of the implicit surface 'phi'
!
!	input:  	implicit surface phi NZxNXxNY
!	output:  	phi_grad NZxNXxNY
!
!###############################################################################

program PHI_SLOPE
	use sep
	use gradient_mod
	implicit none

	!===========================================================================
	!	Allocate and Initialize
	!---------------------------------------------------------------------------
	integer                             :: nz,nx,ny,ix,iz,iy,i
	real								:: dz,dx,oz,ox,rr
	real,dimension(:,:,:),allocatable	:: inphi, binZ, binX, binY, slope
	logical								:: verbose
	integer,dimension(8)               	:: timer0,timer1,timeseg
	real                                :: timer_sum=0.
	!===============================================================================

	call sep_init()
	call DATE_AND_TIME(values=timer0)
	call from_history('n1',nz)
	call from_history('n2',nx)
	call from_history('n3',ny,1)
	call from_param('verbose',verbose,.False.)

	! Allocate
	allocate(slope(nz,nx,ny))
	allocate(inphi(nz,nx,ny))
	allocate(binZ(nz,nx,ny))
	allocate(binX(nz,nx,ny))

	! Initialize
	call sep_read(inphi)

	!##########################################################################################
	!	MAKE GRADIENT
	! dz gradient
	call gradient_init(nz,1.0)
	do iy=1,ny
		do ix=1,nx
			call gradient_op(.false.,.false.,inphi(:,ix,iy),binZ(:,ix,iy))
		enddo
	enddo
	! dx gradient
	call gradient_init(nx,1.0)
	do iy=1,ny
		do iz=1,nz
			call gradient_op(.false.,.false.,inphi(iz,:,iy),binX(iz,:,iy))
		enddo
	enddo
	! dy gradient
	if (ny>2) then ! Stencil is +/- 1 from center,so needs 3 points
		allocate(binY(nz,nx,ny))
		call gradient_init(ny,1.0)
		do ix=1,nx
			do iz=1,nz
				call gradient_op(.false.,.false.,inphi(iz,ix,:),binY(iz,ix,:))
			enddo
		enddo
		slope = sqrt(binZ**2 + binX**2 + binY**2)
	else
		write(0,*) ">>>> 2D model detected <<<<"
		slope = sqrt(binZ**2 + binX**2)
	endif
	!##########################################################################################


	call DATE_AND_TIME(values=timer1)
	timer_sum=0.0
	timeseg = timer1-timer0
	timer_sum = timer_sum + (((timeseg(3)*24+timeseg(5))*60+timeseg(6))*60+timeseg(7))*1000+timeseg(8)
	write(0,*) "Total gradient calc time: ",timer_sum/1000
	

	! Write outputs
	call to_history("n1"    ,nz		)
	call to_history("n2"    ,nx		)
	call to_history("n3"    ,ny		)
	call to_history("label1",'z [km]')
	call to_history("label2",'x [km]')
	call to_history("label3",'y [km]')
	call sep_write(slope)

end program
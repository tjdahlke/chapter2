module tomo_mod

implicit none

contains

!!---------------------------------------------------------- 

subroutine AbcWt(vel, vwt, wt, dt, d1, d2, bc1, bc2, bc3, bc4)
  !! compute absorbing boundary condition weights
  integer                       :: bc1, bc2, bc3, bc4
  real                          :: dt, d1, d2
  real, dimension(:,:)          :: vel, vwt, wt
  integer                       :: iz, ix, n1, n2

  n1 = size(vel,1)
  n2 = size(vel,2)

  vwt = 0.
  wt = 0.

  do ix=1,n2
    do iz=1,bc1
      vwt(iz,ix) = vel(iz,ix)*dt/d1
      wt(iz,ix) = wt(iz,ix) + float(bc1-iz+1)/float(bc1)*.15*vwt(iz,ix)
    enddo
  enddo

  do ix=1,n2
    do iz=n1-bc3+1,n1
      vwt(iz,ix) = vel(iz,ix)*dt/d1
      wt(iz,ix) = wt(iz,ix) + float(iz-n1+bc3)/float(bc3)*.15*vwt(iz,ix)
    enddo
  enddo

  do ix=1,bc2
    do iz=1,n1
      vwt(iz,ix) = vel(iz,ix)*dt/d2
      wt(iz,ix) = wt(iz,ix) + float(bc2-ix+1)/float(bc2)*.15*vwt(iz,ix)
    enddo
  enddo

  do ix=n2-bc4+1,n2
    do iz=1,n1
      vwt(iz,ix) = vel(iz,ix)*dt/d2
      wt(iz,ix) = wt(iz,ix) + float(ix-n2+bc4)/float(bc4)*.15*vwt(iz,ix)
    enddo
  enddo

end subroutine

!!---------------------------------------------------------- 

subroutine stepper(r0, r1, r2, r3, step_size, stat)
  !! testing step sizes
  double precision              :: r0, r1, r2, r3, step_size
  integer                       :: stat

  if(isnan(r1) .or. r1 .eq. r1 + r1) then
    write(0,*) "something wrong with r1, will reset its value"
    r1 = r0 + abs(r0)
  endif
  if(isnan(r2) .or. r2 .eq. r2 + r2) then
    write(0,*) "something wrong with r2, will reset its value"
    r2 = r0 + abs(r0)
  endif
  if(isnan(r3) .or. r3 .eq. r3 + r3) then
    write(0,*) "something wrong with r3, will reset its value"
    r3 = r0 + abs(r0)
  endif

  if(r0 < r1 .and. r0 < r2 .and. r0 < r3) then
    write(0,*) "all residuals are larger than r0, will try dividing by 4"
    step_size = step_size * 0.25
    stat = 1
  elseif(r1 < r2 .and. r1 < r3) then
    write(0,*) "step scalar is = 0.5"
    step_size = step_size * 0.5
    write(0,*) "The best step size is = ", step_size
    stat = 0
  elseif(r2 < r1 .and. r2 < r3) then
    write(0,*) "step scalar is = 1.0"
    write(0,*) "The best step size is = ", step_size
    stat = 0
  elseif(r3 < r1 .and. r3 < r2) then
    write(0,*) "step scalar is = 1.5"
    step_size = step_size * 1.5
    write(0,*) "The best step size is = ", step_size
    stat = 0
  else
    write(0,*) "unknown step size problem, will try dividing by 4"
    step_size = step_size * 0.25
    stat = 1
  endif

end subroutine

end module

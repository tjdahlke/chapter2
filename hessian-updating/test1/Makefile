include ${SEPINC}/SEP.top
main=$(shell pwd | rev | cut -c 7- | rev)
project=$(shell pwd)
INVname=single-canyon-GN
genpar=${P}/sigsbee_canyon.p
include ${main}/ParamMakefile
include ${main}/BuildMakefile

#################################################################################
#		FIGURES

default: ${R}/centershot-analysis-single.v ${R}/single-final-gn.v  ${R}/single-guess.v  ${R}/single-pert.v ${R}/single-negative-gradient.v

${R}/sigsbee-full.v:
	Grey title=" " label3="velocity [km/s]" color=jc wantscalebar=y newclip=0 < ${truevel} ${FIG}$@

${R}/single-obsdata.v: ${obs_path}
	Window3d n3=1 f3=19 max1=2.0 < $< > $@1
	echo "label3=' ' " >> $@1
	< $@1 Grey newclip=1 bclip=-0.02 eclip=0.02 title=" " wantscalebar=y ${FIG}$@
	rm $@1

${R}/single-residual.v: ${res_path}
	Window3d n3=1 f3=19 max1=2.0 < $< > $@1
	echo "label3=' ' " >> $@1
	< $@1 Grey newclip=1 bclip=-0.0002 eclip=0.0002 title=" " wantscalebar=y ${FIG}$@
	rm $@1

${R}/centershot-analysis-single.v: ${R}/single-obsdata.v ${R}/single-residual.v 
	vp_SideBySideAniso $^ > $@

${R}/single-final-gn.v: single-canyon-GN.H
	Window n2=1 f2=24 < model${INVname}.H > tmp.h ; \
	${B}/APPLY_Dop_RBF1.x rbftable=${rbftable} rbfcoord=${rbfcoord} masksalt=${masksalt} \
	scaling=${scale_path} verbose=0 adjoint=0 par=${genpar} < tmp.h > $@1
	echo "label3=' ' ">> $@1
	Window3d ${greywin} < $@1 | Grey title=" " gainpanel=e color=jc newclip=0 pclip=100 wantscalebar=y ${FIG}$@
	rm $@1

${R}/single-guess.v: ${truevel}
	Window3d ${greywin} < $< | Grey title=" " label3="velocity [km/s]" color=jc wantscalebar=y pclip=100 ${FIG}$@

${R}/single-pert.v: deltaM1.H
	Window3d ${greywin} < $< | Grey title=" " label3="velocity [km/s]" color=jc wantscalebar=y pclip=100 ${FIG}$@

${R}/objfunc-single-gn.v: single-canyon-GN.H
	echo 'unit1=  unit2=  label1="iteration number" label2="objective function value" ' >>objsingle-canyon-GN.H
	sfgraph title=" " < objsingle-canyon-GN.H >$@

${R}/single-negative-gradient.v: ${rtm_path}comp ${masksalt} ${scale_path}
	Math file1=${rtm_path}comp file2=${masksalt} file3=${scale_path} exp='file1*file2*file3' > $@1
	Window3d ${greywin} < $@1 | Grey title=" " label3=" " color=jc wantscalebar=y pclip=100 ${FIG}$@
	rm $@1


#################################################################################
#		 WORKFLOWS
single-canyon-GN.H: ${rbf_path} ${B}/MAKE_MASK_RBF.x ${B}/MAXBETA_RBF.x ${obs_path} \
	${B}/APPLY_Dop_RBF1.x ${B}/APPLY_RBF.x ${B}/HESSIAN_GN.x \
	${vel_path} ${B}/FBI.x
	python ${SCRIPTDIR}/workflow_hess_RBF.py ${inversion_PBS} ${Hessian_PBS} obs_path=${obs_path} gnhess=1 hess_out_path=$@ inversionname=${INVname} 


#################################################################################

deltaM1.H: ${truevel} ${vel_path}
	Window3d f3=0 n3=1 < ${vel_path}comp > tmp.h
	Add scale=1,-1 < ${truevel} tmp.h >$@
	rm tmp.h;

# Do single canyon perturbation to the side (left side of canyon)
top=1.19999
bot=1.500000001
mini=0.00000001
mid=0.85
tope=$(shell echo ${top}+${mini} | bc)
bote=$(shell echo ${bot}-${mini} | bc)
mide=$(shell echo ${mid}-0.001 | bc)
${truesaltphi}: ${phi_path} ${B}/PHI0_BUILDER_BINARY.x
	Window3d min1=${tope} max1=${bote} 				< $< > noshift1.h
	Window3d min1=${tope} max1=${bote} n2=393 < $< > shift1.h
	< shift1.h Pad beg2=1 extend=1 > shifted.hA
	Window3d max2=${mide} < shifted.hA > shifted.h
	Window3d min2=${mid} < noshift1.h > noshifted.h
	Cat axis=2 shifted.h noshifted.h > center.h
	Window3d max1=${top} < $< > top.h
	Window3d min1=${bot} < $< > bot.h
	Cat axis=1 top.h center.h bot.h > tmp.h
	${B}/PHI0_BUILDER_BINARY.x < tmp.h thresh=0.0 > $@;
	rm top.h bot.h tmp.h shifted.h shift1.h noshift1.h noshifted.h center.h shifted.hA


#################################################################################

burn:
	rm -f *.h
	rm -f ${pathtempinv}/*
	rm -f ${pathtemphess}/*
	rm -f ${pathtemprbf}/*
	rm -f *.B
	rm -f *.tmp
	rm -f wrk/*
	rm -f *.Hp
	rm -f log*
	rm -f *.H*
	rm -f ${SCRIPTDIR}/*.pyc
	rm -f *.txt
	rm -f core.*
	rm -f .make.dependencies.LINUX
	rm -rf scratch/*
	rm -f *.log


include ${SEPINC}/SEP.bottom

#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess

from batch_task_executor import *
import pbs_util

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class HESSFULL_OP(BatchTaskComposer):
    def __init__(self, param_reader):
        BatchTaskComposer.__init__(self)

        # Read in the job parameters
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.jobprefix = param_reader.prefix
        self.vel_path = self.dict_args['vel_path']
        self.hess_in_path = self.dict_args['hess_in_path']
        self.genpar = self.dict_args['genpar']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.wavelet = self.dict_args['wavelet']
        self.res_path = self.dict_args['res_path']
        self.rbf_path = self.dict_args['rbf_path']
        self.maskvelb = self.dict_args['maskvelb']
        self.masksalt = self.dict_args['masksalt']
        self.scaling = self.dict_args['scaling']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots / float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None] * self.njobs
        # List of actual shot number (starting @ 0)
        self.subjobIND_list = range(
            self.ish_beg, self.nshots + 1, self.nsh_perjob)

        # Make a list of all the files for each job ( shots per job)
        for i in range(0, self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots - ish_start)
            self.subjobfns_list[i] = [None] * nsh
            for j in range(0, nsh):
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (
                    self.path_out, self.jobprefix, ish_start + j + 1)
        return


    def GetSubjobScripts(self, subjobid):
        # Return the scripts content for that subjob.
        subjobfns = self.subjobfns_list[subjobid]
        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns)  # number of files

        for fn in subjobfns:
            shot = self.nsh_perjob * subjobid + cnt + 1
            cnt += 1
            if cnt > nf_act:
                break
            selectres = "Window3d n3=1 f3=%s < %s > %s/singleres-%s.H" % (
                int(shot - 1), self.res_path, self.path_out, shot)
            derivop1 = "%s/APPLY_Dop_RBF1.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=1 adjoint=0 par=%s < %s > %s/fullmodFULLin%s_%s.H" % (
                self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.hess_in_path, self.path_out, shot, self.jobprefix)
            operator = "%s/%s verbose=1 adj=0" % (self.T_BIN, 'HESSIAN_FULL.x')
            inputs1 = 'par=%s isou=%s  data=%s/singleres-%s.H' % (
                self.genpar, shot, self.path_out, shot)
            inputs2 = 'wavelet=%s velmod=%s srcwave_in=%s/%s_%s.H ' % (
                self.wavelet, self.vel_path, self.path_out, self.fullsrcwave_name, shot)
            outputs = '< %s/fullmodFULLin%s_%s.H > %s  gncomp=%s/gncomp%s_%s.h  wemvacomp=%s/wemvacomp%s_%s.h' % (
                self.path_out, shot, self.jobprefix, fn, self.path_out, shot, self.jobprefix, self.path_out, shot, self.jobprefix)
            cmd = '%s; %s; %s %s %s %s;\n' % (
                selectres, derivop1, operator, inputs1, inputs2, outputs)
            # Note: need the \n at the end... also cant use < in string itself
            scripts.append(cmd)
        return scripts, str(subjobid)

    def GetSubjobsInfo(self):
        '''Should return two lists, subjobids and subjobfns.'''
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)


class make_hessFULL(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out
        self.jobprefix = param_reader.prefix
        self.nshots = int(dict_args['nfiles'])
        self.masksalt = dict_args['masksalt']
        self.scaling = dict_args['scaling']
        self.rbfcoord = dict_args['rbfcoord']
        self.rbftable = dict_args['rbftable']
        self.T_BIN = dict_args['T_BIN']
        self.genpar = dict_args['genpar']

        allGN='%s/ALLgncomp.h' % (self.path_out)
        allWEMVA='%s/ALLwemvacomp.h' % (self.path_out)
        FullModOut="%s/FullModOut_%s.H" % (self.path_out, self.jobprefix)

        #----- FULL HESSIAN APPLIED TO RTM GRADIENT --------------------------------------------
        job5 = HESSFULL_OP(param_reader)
        bte = BatchTaskExecutor(param_reader)
        prefix = param_reader.prefix
        print "--------- Submitting/Running FULL HESSIAN ---------------------------"
        bte.LaunchBatchTask(prefix, job5)

        #------ Combine the GN files into one single file -----------------------------------------
        args = ''
        for shot in range(1, self.nshots+1):
            args = '%s %s/gncomp%s_%s.h' % (args, self.path_out, shot, self.jobprefix)
        cmd = 'Cat %s | Transp plane=23 | Stack maxsize=1000 > %s out=%s@ ' % (args, allGN,allGN)
        print "--------- Stacking GN component files  ------------------------------"
        print(cmd)
        subprocess.call(cmd, shell=True)

        #------ Combine the WEMVA files into one single file -----------------------------------------
        args = ''
        for shot in range(1, self.nshots+1):
            args = '%s %s/wemvacomp%s_%s.h' % (args, self.path_out, shot, self.jobprefix)
        cmd = 'Cat %s | Transp plane=23 | Stack maxsize=1000 > %s1 out=%s1@ ' % (args, allWEMVA, allWEMVA)
        print "--------- Stacking WEMVA component files  ------------------------------"
        print(cmd)
        subprocess.call(cmd, shell=True)

        #------ Get Max absolute values from GN and WEMVA components ----------------------------------------------
        cmd1="Math file1=%s exp='@ABS(file1)' | Attr param=1 | Get parform=n maxval" % (allGN)
        cmd2="Math file1=%s1 exp='@ABS(file1)' | Attr param=1 | Get parform=n maxval" % (allWEMVA)
        ps1 = subprocess.Popen(cmd1,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        maxGN = float(ps1.communicate()[0])
        print maxGN
        ps2 = subprocess.Popen(cmd2,shell=True,stdout=subprocess.PIPE,stderr=subprocess.STDOUT)
        maxWEMVA = float(ps2.communicate()[0])
        print maxWEMVA
        print(">>>>>>>>>> WEMVA is %s percent of GN \n" % (100*maxWEMVA/maxGN))
        text_fileA = open("wemva_magnitude_percentage.txt", "a")
        text_fileA.write("%s\n" % (100*maxWEMVA/maxGN))
        text_fileA.close()

        # Reduce the WMEVA component if its too strong
        WEMVAfactor=99999.0
        if ((WEMVAfactor*maxGN) < maxWEMVA): #WEMVA component is too strong
            print(">>>>>>>>>> WEMVA too strong; SCALING IT BACK to %s of GN component value <<<<<<<<<<<<<\n" % (WEMVAfactor))
            cmd = "Scale < %s1 > tj; Math file1=tj exp='file1*%s' > %s" % (allWEMVA,(WEMVAfactor*maxGN),allWEMVA)
            print(cmd)
            subprocess.call(cmd, shell=True)
        else:
            print(">>>>>>>>>> WEMVA component is sufficiently small. Percentage of GN magnitude: %s <<<<<<<<<<<<<\n" % (100*maxWEMVA/maxGN))
            cmd = "Cp %s1 %s" % (allWEMVA,allWEMVA)
            print(cmd)
            subprocess.call(cmd, shell=True)

        # Sum the results together
        cmd = "Add %s %s > %s" % (allGN, allWEMVA, FullModOut)
        # cmd = "Cp %s %s" % (allGN, FullModOut)
        print(cmd)
        subprocess.call(cmd, shell=True)


        # # Dont use the WMEVA component if it's too strong
        # WEMVAfactor=0.25
        # if ((WEMVAfactor*maxGN) < maxWEMVA): #WEMVA component is too strong
        #     print(">>>>>>>>>> WEMVA too strong; Not using it <<<<<<<<<<<<<\n")
        #     # Just use the GN component
        #     cmd = "Cp %s %s" % (allGN, FullModOut)
        #     print(cmd)
        #     subprocess.call(cmd, shell=True)
        # else:
        #     print(">>>>>>>>>> WEMVA component is sufficiently small. Percentage of GN magnitude: %s <<<<<<<<<<<<<\n" % (100*maxWEMVA/maxGN))
        #     # Sum the results together
        #     cmd = "Add %s %s > %s" % (allGN, allWEMVA, FullModOut)
        #     print(cmd)
        #     subprocess.call(cmd, shell=True)


        # Apply the derivoperator on the full hessian
        derivop2 = "%s/APPLY_Dop_RBF1.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=1 adjoint=1 par=%s < %s > %s" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, FullModOut, self.hess_out_path)
        print(derivop2)
        subprocess.call(derivop2, shell=True)

        #----- Removing ADJOINT and FORWARD FULL HESSIAN output files from wrk directory -------------
        args_hess = ''
        fn_seph_list = ' '
        _, fns_list = job5.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        for i in range(0, len(fn_seph_list)):
            args_hess = '%s %s' % (args_hess, fn_seph_list[i])
        cmd1 = 'rm -rf %s' % (args_hess)
        cmd2 = 'rm -rf %s/tmp*' % (self.path_out)
        cmd3 = 'rm -rf %s/singleres*' % (self.path_out)
        # cmd4 = 'rm -rf %s %s %s' % (allGN,allWEMVA,FullModOut)
        # cmd = '%s; %s; %s; %s;' % (cmd1, cmd2, cmd3, cmd4)
        cmd = '%s; %s; %s;' % (cmd1, cmd2, cmd3)
        subprocess.call(cmd, shell=True)
        print "--------- Removing ADJOINT and FORWARD FULL HESSIAN output files from wrk directory  ------------------------------"
        print(cmd)

        return


if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessFULL(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)

from modeled_data import *
from born import *
import subprocess



class GRADMAKER(object):
    def __init__(self, param_reader):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.res_path = self.dict_args['res_path']
        self.obs_path = self.dict_args['obs_path']
        self.syn_path = self.dict_args['syn_path']
        self.rtm_path = self.dict_args['rtm_path']
        self.vel_path = self.dict_args['vel_path']
        return

    def syn_data(self,param_reader,debug,prefixA,vel_path,syn_path):
        #----- MAKE SYNTHENTIC DATA -------------------
        syndata = data_modeling(param_reader,vel_path,prefixA)
        bte = BatchTaskExecutor(param_reader)
        if (debug):
            print('-----------------------------------------------------------------')
            print "MAKING SYNTHENTIC DATA "
        param_reader.prefix=prefixA
        bte.LaunchBatchTask(prefixA, syndata)
        # Combine the files to one single file.
        _, fns_list = syndata.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args=''
        for i in range(0,len(fn_seph_list)):
          args = '%s %s' % (args,fn_seph_list[i])
        cmd = 'Cat %s > %s' % (args,syn_path)
        if (debug):
            print('-----------------------------------------------------------------')
            print " CONCATENATING SYNTHETIC FORWARD DATA FILES"
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def residual(self, param_reader,debug,syn_path,res_path):
        # #----- MAKE RESIDUAL  ------------------------------------------
        cmd = "Math file1=%s file2=%s exp='(file1-file2)' > %s" % (syn_path,self.obs_path,res_path) # Need residual to mave meters in d2 and o2 for MUTE_DA to work properly
        if (debug):
            print('-----------------------------------------------------------------')
            print " COMPUTING RESIDUAL "
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def born_full(self, param_reader,debug,gradtag,res_path,rtm_path):
        #----- ADJOINT BORN APPLIED TO RESIDUAL DATA -------------------
        modelpath='dummy_string'
        job5 = BORN(param_reader, self.vel_path, gradtag, 1.0, res_path, modelpath,debug,'full')
        bte = BatchTaskExecutor(param_reader)
        if (debug):
            print('-----------------------------------------------------------------')
            print " RUNNING BORN ADJOINT"
        prefixA="BORN_OPERATOR"
        param_reader.prefix=prefixA
        bte.LaunchBatchTask(prefixA, job5)
        # Combine the files to one single file.
        _, fns_list = job5.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args=''

        for i in range(0,len(fn_seph_list)):
          args = '%s %s' % (args,fn_seph_list[i])
        cmd = 'Cat axis=4 %s | Transp plane=24 | Stack maxsize=1000 | Transp plane=23 > %s out=%s@ ' % (args,rtm_path,rtm_path)
        # cmd = 'Cat axis=4 %s | Transp plane=24 | Stack maxsize=1000 | Transp plane=23 |Scale > %s out=%s@ ' % (args,self.rtm_path,self.rtm_path)
        if (debug):
            print('-----------------------------------------------------------------')
            print " STACKING ADJOINT BORN FILES"
            print(cmd)
        subprocess.call(cmd,shell=True)
        return

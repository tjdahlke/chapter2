#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from main_functions import *
from inversion import *
from gradmaker import *

#----------------------------------------------------------------------------------
#   NOTE ON EIGENSHIFTING:
#
#   I'm pretty sure that only the seven highest order digits of this are actually used in Solver_ops.
#   So if the negative eigenvalue is smaller than that, I wont be able to discern it.
#   0.0000001 seems to be the smallest scaling factor that actually affects things when using Solver_ops
#----------------------------------------------------------------------------------


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    debug = True

    inversionname = param_reader.dict_args['inversionname']
    vel_path = param_reader.dict_args['vel_path']
    syn_path = param_reader.dict_args['syn_path']
    res_path = param_reader.dict_args['res_path']
    rtm_path = param_reader.dict_args['rtm_path']
    phi_path = param_reader.dict_args['phi_path']
    masksalt = param_reader.dict_args['masksalt']
    maskvelb = param_reader.dict_args['maskvelb']
    pathtemp = param_reader.dict_args['pathtemp']
    sparsemodel = param_reader.dict_args['sparsemodel']

    phigrad = param_reader.dict_args['phigrad']
    tomograd = param_reader.dict_args['tomograd']
    pathtemphess = param_reader.dict_args['pathtemphess']
    eigenshift = float(param_reader.dict_args['eigenshift'])
    gnhess = int(param_reader.dict_args['gnhess'])

    # INITIALIZING THE MAIN FUNCTION SPACES. FUNCTIONS THAT CALL OTHER FUNCTIONS RELY ON THESE OBJECT INSTANCES
    main = MAIN_FUNCTIONS(param_reader)
    gradient_maker = GRADMAKER(param_reader)
    inversion = inversion(param_reader)

    ###################################################################################################

    #------- MAKE SYNTHETIC DATA AND RESIDUAL -------------------------------------------------
    gradient_maker.syn_data(param_reader, debug,"syndata_MAIN", vel_path, syn_path)
    gradient_maker.residual(param_reader, debug, syn_path, res_path)

    print(" ====================================================================================")
    #------- MAKE FULL RTM GRADIENT ------------------------------------------
    gradtag = 'primary_gradient'
    gradient_maker.born_full(param_reader, debug, gradtag, res_path, rtm_path)

    #------- MAKE MASKS FOR D OPERATOR --------------------------------------------
    main.MAKE_MASKS_RBF(debug,phi_path,masksalt,maskvelb)

    #------- MAKE SCALING --------------------------------------------------
    main.MAKE_SCALING(debug)

    #----- Calc phi_grad & b_grad ------------------------------------------
    main.DELTAP_BEFORE_RBF1(debug,rtm_path,sparsemodel)

    print(" ====================================================================================")
    #--------- POWER ITERATION --------------------------------
    inversion.power_iterationRBF(param_reader, debug, sparsemodel, 50, inversionname, eigenshift)


#!/bin/tcsh
#PBS -N jobname
#PBS -l nodes=1:ppn=4
#PBS -q sep
#PBS -V
#PBS -j oe
#PBS -e /data/biondo/tjdahlke/research/wrk
#PBS -o /data/biondo/tjdahlke/research/wrk
#PBS -d /data/biondo/tjdahlke/research/wrk

#
cd $PBS_O_WORKDIR
setenv OMP_NUM_THREADS 4
echo $OMP_NUM_THREADS
#

echo WORKDIR is: $PBS_O_WORKDIR
sleep 5
echo NodeFile is: $PBS_NODEFILE
echo hostname is: $HOSTNAME

#set TMPDIR=/tmp
#set HOSTFILE=hostfile_${PBS_JOBID}
#echo hostfile is: ${HOSTFILE}
#cat ${PBS_NODEFILE} | sort | uniq > ${HOSTFILE}

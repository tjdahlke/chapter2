from modeled_data import *
from born import *
import subprocess
import os.path
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math


class inversion(object):
    def __init__(self, param_reader):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.res_path = self.dict_args['res_path']
        self.obs_path = self.dict_args['obs_path']
        self.syn_path = self.dict_args['syn_path']
        self.rtm_path = self.dict_args['rtm_path']
        self.vel_path = self.dict_args['vel_path']
        self.pathtemphess = self.dict_args['pathtemphess']
        self.gensolve_F = "%s/forward_FULLhessPBS.txt" % (self.pathtemphess)
        self.gensolve_A = "%s/adjoint_FULLhessPBS.txt" % (self.pathtemphess)
        self.solverpath = self.dict_args['solverpath']
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nfiles = int(self.dict_args['nfiles'])  # Same as number of shots
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.genpar = self.dict_args['genpar']
        self.pbs_template = self.dict_args['pbs_template']
        self.prefix = self.dict_args['prefix']
        self.nfiles_perjob = int(self.dict_args['nfiles_perjob'])
        self.hess_iter = self.dict_args['hess_iter']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.rbf_path = self.dict_args['rbf_path']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.masksalt = self.dict_args['masksalt']
        self.maskvelb = self.dict_args['maskvelb']
        self.scaling = self.dict_args['scaling']
        self.phigrad = self.dict_args['phigrad']
        self.tomograd = self.dict_args['tomograd']
        self.rbftable = self.dict_args['rbftable']
        self.queues = self.dict_args['queues']
        self.queues_cap = self.dict_args['queues_cap']
        self.pypath = self.dict_args['pypath']
        self.outputfilename = self.dict_args['inversionname']
        return

    def hessFULL_RBF1(self, debug, invname):
        text_fileA = open(self.gensolve_F, "w")
        text_fileF = open(self.gensolve_A, "w")
        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = "fullsrcwave_name=%s res_path=%s" % (
            self.fullsrcwave_name, self.res_path)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        args4f = "jobprefix=%s fullrecwave_name=%s hess_in_path=input.H hess_out_path=output.H" % (
            invname, self.fullrecwave_name)
        args5f = "rbftable=%s maskvelb=%s masksalt=%s scaling=%s rbf_path=%s rbfcoord=%s" % (
            self.rbftable, self.maskvelb, self.masksalt, self.scaling, self.rbf_path, self.rbfcoord)
        # cmdF = "RUN: hessian_FULL_RBF1.py %s %s %s %s %s >> hessian_FULL_RBF1.log" % (
        cmdF = "RUN: python %s/hessian_FULL_RBF1mod.py %s %s %s %s %s >> hessian_FULL_RBF1mod.log" % (
            self.pypath, args1f, args2f, args3f, args4f, args5f)
        text_fileA.write("%s" % cmdF)
        text_fileA.close()
        text_fileF.write("%s" % cmdF)
        text_fileF.close()
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return



    def hessGN_RBF1(self, debug, invname):
        text_fileA = open(self.gensolve_F, "w")
        text_fileF = open(self.gensolve_A, "w")
        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = "jobprefix=%s fullrecwave_name=%s fullsrcwave_name=%s res_path=%s" % (
            invname, self.fullrecwave_name, self.fullsrcwave_name, self.res_path)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s queues=%s queues_cap=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg, self.queues, self.queues_cap)
        args4f = "hess_in_path=input.H hess_out_path=output.H"
        args5f = "rbftable=%s maskvelb=%s masksalt=%s scaling=%s rbf_path=%s rbfcoord=%s" % (
            self.rbftable, self.maskvelb, self.masksalt, self.scaling, self.rbf_path, self.rbfcoord)
        cmdF = "RUN: python %s/hessian_GN_RBF1.py %s %s %s %s %s >> hessian_GN_RBF1.log" % (
            self.pypath, args1f, args2f, args3f, args4f, args5f)
        text_fileA.write("%s" % cmdF)
        text_fileA.close()
        text_fileF.write("%s" % cmdF)
        text_fileF.close()
        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdF)
        return



    def hessMAIN(self, param_reader, debug, invname, GNhess, RBF, phiANDvelb, gradtag, symmetric_in, inputmodel, hess_out_path):
        #-------------------------------------------------------------------------------------
        # Write the FORWARD and ADJOINT .txt files for the generic solver
        if (GNhess):
            if (RBF):
                self.hessGN_RBF1(debug, invname)
            else:
                self.hessGN_PLAIN(debug, invname)
        else:
            if (phiANDvelb):
                self.hessFULL_RBF2(debug, invname, gradtag)
            else:
                self.hessFULL_RBF1(debug, invname)

        # Make input data
        cmd = "Cp %s %s/inputdata.H" % (inputmodel, self.pathtemphess)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making input data"
            print(cmd)
        subprocess.call(cmd, shell=True)

        # Make a zero initial model
        cmd = "Cp %s/inputdata.H %s/initmodel.H; Solver_ops file1=%s/initmodel.H op=zero;" % (
            self.pathtemphess, self.pathtemphess, self.pathtemphess)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making a zero initial model"
            print(cmd)
        subprocess.call(cmd, shell=True)

        if (symmetric_in):
            symmetric = 'yes'
        else:
            symmetric = 'no'

        if(not os.path.isfile(hess_out_path)):
            # CG SOLVE
            cmd1 = "python %s/python_solver/generic_linear_prob.py  fwd_cmd_file=%s  adj_cmd_file=%s iteration_movies=obj,model,gradient,residual " % (
                self.solverpath, self.gensolve_F, self.gensolve_A)
            cmd2 = "debug=no data=%s/inputdata.H init_model=%s/initmodel.H  niter=%s  inv_model=%s suffix=%s symmetric=%s  dotprod=0" % (
                self.pathtemphess, self.pathtemphess, self.hess_iter, hess_out_path, invname, symmetric)
            cmd = "%s %s" % (cmd1, cmd2)  # tolobjrel=0.05
            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " LAUNCHING GENERIC PYTHON SOLVER FOR HESSIAN"
                print(cmd)
            subprocess.call(cmd, shell=True)
        else:
            print "------------------------------------------------------------"
            print "Inverted result already exists: Skipping Hessian Inversion"

        return



    def power_iterationRBF(self, param_reader, debug, inputmodel, num_simulations, invname, eigenshift):

        hessin = "%s/eigenvectorIN.h" % (self.path_out)
        hessout = "%s/eigenvectorOUT.h" % (self.path_out)

        args1f = "pbs_template=%s genpar=%s vel_path=%s path_out=%s T_BIN=%s wavelet=%s" % (
            self.pbs_template, self.genpar, self.vel_path, self.path_out, self.T_BIN, self.wavelet)
        args2f = " fullsrcwave_name=%s res_path=%s fullrecwave_name=%s jobprefix=%s" % (
            self.fullsrcwave_name, self.res_path, self.fullrecwave_name, invname)
        args3f = "nfiles_perjob=%s prefix=%s nfiles=%s nsh_perjob=%s ish_beg=%s" % (
            self.nfiles_perjob, self.prefix, self.nfiles, self.nsh_perjob, self.ish_beg)
        args4f = "hess_in_path=%s hess_out_path=%s" % (hessin, hessout)
        args5f = "rbftable=%s maskvelb=%s masksalt=%s scaling=%s rbf_path=%s rbfcoord=%s" % (
            self.rbftable, self.maskvelb, self.masksalt, self.scaling, self.rbf_path, self.rbfcoord)
        cmdF = "python %s/hessian_FULL_RBF1mod.py %s %s %s %s %s >>hessian_FULL_RBF1_poweriter.log" % (
            self.pypath,args1f, args2f, args3f, args4f, args5f)

        #-------------------------------------------------------------
        # Initialize with random eigenvector
        init1 = "Cp %s %s" % (inputmodel, hessin)
        init2 = "Solver_ops op=rand file1=%s" % (hessin)
        init = "%s; %s" % (init1, init2)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Initializing with random eigenvector"
            print(init)
        subprocess.call(init, shell=True)

        # Remove old power iteration file if it already exists
        outf = open(self.outputfilename, "a+",0) # The "0" argument means it prints instantly (zero buffer)
        outf.write("\n\n")

        #-------------------------------------------------------------
        # Perform power iterations!
        for i in range(num_simulations):
            msg = "iteration = %s" % (i)
            print(msg)

            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " Running a Hessian application"
                print(cmdF)
            subprocess.call(cmdF, shell=True)

            #------ Perform eigenvalue shift -----------------------------------------
            cmd = "Solver_ops file1=%s file2=%s scale1_r=1.0 scale2_r=%s op=scale_addscale" % (
                hessout, hessin, eigenshift)
            print "--------- Do eigenshifting  ------------------------------"
            print(cmd)
            subprocess.call(cmd, shell=True)

            #------ Do Rayleigh quotient numerator & denominator ---------------------
            print('---------------------------------------------------------------------')
            file1 = "file1=%s" % (hessin)
            file2 = "file2=%s" % (hessout)
            cmd1 = ['Solver_ops', 'op=dot', file1]
            cmd2 = ['Solver_ops', 'op=dot', file1, file2]
            out1 = subprocess.Popen(
                cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
            out2 = subprocess.Popen(
                cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
            denom = float(out1.split()[2])
            numer = float(out2.split()[2])
            print(numer)
            print(denom)
            print(numer/denom)

            # Write the rayleigh quotient to file
            outf.write("%.12f  " % float(numer/denom))

            # Normalize by L2 norm
            print('---------------------------------------------------------------------')
            file1 = "file1=%s" % (hessout)
            cmd1 = ['Solver_ops', 'op=dot', file1]
            out1 = subprocess.Popen(
                cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
            normval = float(out1.split()[2])
            cmdnorm = "Math file1=%s exp='file1/%s' > %snormalized" % (
                hessout, normval, hessout)

            # Perform the copy
            cmdcp = "Cp %snormalized %s" % (hessout, hessin)
            cmd = "%s; %s" % (cmdnorm, cmdcp)
            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " Normalize Hessian output and reset to being the input"
                print(cmd)
            subprocess.call(cmd, shell=True)

            # # Saving intermediate outputs
            # if (i==0):
            #     cmd = "Cp %s %scomp" % (hessout,hessout)
            # else:
            #     cmd1 = "Cat %scomp %s > %snew" % (hessout,hessout,hessout)
            #     cmd2 = "Rm %scomp" % (hessout)```
            #     cmd3 = "mv %snew %scomp" % (hessout,hessout)
            #     cmd = "%s; %s; %s" % (cmd1, cmd2, cmd3)
            # if (debug):
            #     print('-----------------------------------------------------------------')
            #     print(cmd)
            # subprocess.call(cmd,shell=True)
        outf.close()

        return

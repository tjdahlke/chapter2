#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess

from batch_task_executor import *
import pbs_util

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class HESSFULL_OP(BatchTaskComposer):
    def __init__(self, param_reader):
        BatchTaskComposer.__init__(self)

        # Read in the job parameters
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.jobprefix = param_reader.prefix
        self.vel_path = self.dict_args['vel_path']
        self.hess_in_path = self.dict_args['hess_in_path']
        self.genpar = self.dict_args['genpar']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.wavelet = self.dict_args['wavelet']
        self.res_path = self.dict_args['res_path']
        self.rbf_path = self.dict_args['rbf_path']
        self.maskvelb = self.dict_args['maskvelb']
        self.masksalt = self.dict_args['masksalt']
        self.scaling = self.dict_args['scaling']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots / float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None] * self.njobs
        # List of actual shot number (starting @ 0)
        self.subjobIND_list = range(
            self.ish_beg, self.nshots + 1, self.nsh_perjob)

        # Make a list of all the files for each job ( shots per job)
        for i in range(0, self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots - ish_start)
            self.subjobfns_list[i] = [None] * nsh
            for j in range(0, nsh):
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (
                    self.path_out, self.jobprefix, ish_start + j + 1)
        return

    def GetSubjobScripts(self, subjobid):
        # Return the scripts content for that subjob.
        subjobfns = self.subjobfns_list[subjobid]
        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns)  # number of files

        for fn in subjobfns:
            shot = self.nsh_perjob * subjobid + cnt + 1
            cnt += 1
            if cnt > nf_act:
                break
            selectres = "Window3d n3=1 f3=%s < %s > %s/singleres-%s.H" % (
                int(shot - 1), self.res_path, self.path_out, shot)
            derivop1 = "%s/APPLY_Dop_RBF1.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=1 adjoint=0 par=%s < %s > %s/fullmodFULLin%s_%s.H" % (
                self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.hess_in_path, self.path_out, shot, self.jobprefix)
            operator = "%s/%s verbose=1 adj=0" % (self.T_BIN, 'HESSIAN_FULL.x')
            inputs1 = 'par=%s isou=%s  data=%s/singleres-%s.H' % (
                self.genpar, shot, self.path_out, shot)
            inputs2 = 'wavelet=%s velmod=%s srcwave_in=%s/%s_%s.H ' % (
                self.wavelet, self.vel_path, self.path_out, self.fullsrcwave_name, shot)
            outputs = '< %s/fullmodFULLin%s_%s.H > %s/fullmodFULLout%s_%s.H  gncomp=%s/gncomp%s_%s.h  wemvacomp=%s/wemvacomp%s_%s.h' % (
                self.path_out, shot, self.jobprefix, self.path_out, shot, self.jobprefix, self.path_out, shot, self.jobprefix, self.path_out, shot, self.jobprefix)
            derivop2 = "%s/APPLY_Dop_RBF1.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=1 adjoint=1 par=%s < %s/fullmodFULLout%s_%s.H > %s" % (
                self.T_BIN, self.rbftable, self.rbfcoord, self.masksalt, self.scaling, self.genpar, self.path_out, shot, self.jobprefix, fn)
            # Note: need the \n at the end... also cant use < in string itself.
            cmd = '%s; %s; %s %s %s %s; %s;\n' % (
                selectres, derivop1, operator, inputs1, inputs2, outputs, derivop2)
            scripts.append(cmd)
        return scripts, str(subjobid)

    def GetSubjobsInfo(self):
        '''Should return two lists, subjobids and subjobfns.'''
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)


class make_hessFULL(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out
        fn_seph_list = ' '

        #----- FULL HESSIAN APPLIED TO RTM GRADIENT --------------------------------------------
        job5 = HESSFULL_OP(param_reader)
        bte = BatchTaskExecutor(param_reader)
        prefix = param_reader.prefix
        print "--------- Submitting/Running FULL HESSIAN ---------------------------"
        bte.LaunchBatchTask(prefix, job5)

        #------ Combine the files to one single file -----------------------------------------
        _, fns_list = job5.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args = ''
        for i in range(0, len(fn_seph_list)):
            args = '%s %s' % (args, fn_seph_list[i])
        cmd = 'Cat %s | Transp plane=23 | Stack maxsize=1000 > %s out=%s@ ' % (
            args, self.hess_out_path, self.hess_out_path)
        # Add only does like 5 files!!!! DONT USE Add!!
        print "--------- Stacking FULL HESSIAN files  ------------------------------"
        print(cmd)
        subprocess.call(cmd, shell=True)

        #------ Do Rayleigh quotient numerator & denominator ---------------------
        print('---------------------------------------------------------------------')
        file1 = "file1=%s" % (self.hess_in_path)
        file2 = "file2=%s" % (self.hess_out_path)
        cmd1 = ['Solver_ops', 'op=dot', file1]
        cmd2 = ['Solver_ops', 'op=dot', file1, file2]
        out1 = subprocess.Popen(
            cmd1, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
        out2 = subprocess.Popen(
            cmd2, stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[1]
        denom = out1.split()[2]
        numer = out2.split()[2]
        print(numer)
        print(denom)

        #------ Perform Levenberg-Marquardt shift -----------------------------------------
        scale = 0.00
        eigenshift = scale * float(numer) / float(denom)
        # eigenshift = scale * 2811528435.0
        print(eigenshift)
        cmd = "Solver_ops file1=%s file2=%s scale1_r=1.0 scale2_r=%s op=scale_addscale" % (
            self.hess_out_path, self.hess_in_path, eigenshift)
        print "--------- Do Levenberg-Marquardt shift  ------------------------------"
        print(cmd)
        subprocess.call(cmd, shell=True)

        #----- Removing ADJOINT and FORWARD FULL HESSIAN output files from wrk directory -------------
        ars_hess = ''
        for i in range(0, len(fn_seph_list)):
            ars_hess = '%s %s' % (ars_hess, fn_seph_list[i])
        cmd1 = 'rm -rf %s' % (ars_hess)
        cmd2 = 'rm -rf %s/tmp*' % (self.path_out)
        cmd3 = 'rm -rf %s/singleres*' % (self.path_out)
        cmd = '%s; %s; %s;' % (cmd1, cmd2, cmd3)
        subprocess.call(cmd, shell=True)
        print "--------- Removing ADJOINT and FORWARD FULL HESSIAN output files from wrk directory  ------------------------------"
        print(cmd)

        return


if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessFULL(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)

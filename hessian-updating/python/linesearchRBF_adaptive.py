#!/usr/local/bin/python

import copy, random, os, sepbase, tempfile, time, unittest, math, subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import sys, os
from main_functions import *


#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
class MAIN_LS_FUNCTIONS(object):
    def __init__(self, debug, param_reader, gradinstance, maininstance, lsprefix, Ntries, tomoupd, phiupd):
        self.param_reader=param_reader
        self.dict_args = self.param_reader.dict_args
        self.pathtemp = self.dict_args['pathtemp']
        self.path_out = self.dict_args['path_out']
        self.T_BIN = self.dict_args['T_BIN']
        self.velback = self.dict_args['velback']
        self.drlse_term = self.dict_args['drlse_term']
        self.phi_path = self.dict_args['phi_path']
        self.vsalt = self.dict_args['vsalt']
        self.genpar = self.dict_args['genpar']
        self.rbf_path = self.dict_args['rbf_path']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']
        self.debug=debug
        self.tomoupd=tomoupd
        self.phiupd=phiupd
        self.Ntries=Ntries
        self.lsprefix=lsprefix
        self.gradinstance=gradinstance
        self.maininstance=maininstance
        return

    # Disable printing
    def blockPrint(self):
        sys.stdout = open(os.devnull, 'w')

    # Restore printing
    def enablePrint(self):
        sys.stdout = sys.__stdout__


    def UPDATE_MODEL_LS(self,debug,alpha,beta,phigrad,tomograd,testvelpath):
        #----- UPDATE THE VELOCITY MODEL AND PHI SURFACE -----------------------
        testphimod="%sPHI" % (testvelpath)
        cpp1 = "Cp %s %s" % (self.phi_path,testphimod)
        cpp2 = "Cp %s %s/testvelback.h" % (self.velback,self.path_out)
        add1 = "Solver_ops op=scale_addscale file1=%s file2=%s scale2_r=%s" % (testphimod,phigrad,beta)
        add2 = "Solver_ops op=scale_addscale file1=%s/testvelback.h file2=%s scale2_r=%s" % (self.path_out,tomograd,alpha)
        rbf1 = "%s/PHI0_BUILDER_BINARY.x verbose=1 par=%s < %s phi_out=%s/saltoverlay.H >/dev/null" % (self.T_BIN,self.genpar,testphimod,self.pathtemp)
        rbf2 = "Math file1=%s/saltoverlay.H exp='file1+1.0' | Scale > %s/saltoverlay2.H" % (self.pathtemp,self.pathtemp)
        add3 = "Math file1=%s/saltoverlay2.H file2=%s exp='%f*file1+file2'> %s/unclipppedvel.h" % (self.pathtemp,self.velback,float(self.vsalt),self.pathtemp)
        clip = "Clip clip=%f chop=greater < %s/unclipppedvel.h> %s" % (float(self.vsalt),self.pathtemp,testvelpath)
        rm1 = "rm %s/saltoverlay.H* %s/unclipppedvel.h %s/testvelback.h" % (self.pathtemp,self.pathtemp,self.path_out)
        cmd = "%s; %s; %s; %s; %s; %s; %s; %s; %s;" % (cpp1, cpp2, add1, add2, rbf1, rbf2, add3, clip, rm1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('UPDATING MODEL')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return

    # def UPDATE_MODEL_LS(self,debug,alpha,beta,phigrad,tomograd,testvelpath):
    #     #----- UPDATE THE VELOCITY MODEL AND PHI SURFACE -----------------------
    #     cpp1 = "Cp %s %s/testrbfmod.h" % (self.rbf_path,self.path_out)
    #     cpp2 = "Cp %s %s/testvelback.h" % (self.velback,self.path_out)
    #     add1 = "Solver_ops op=scale_addscale file1=%s/testrbfmod.h  file2=%s scale2_r=%s" % (self.path_out,phigrad,beta)
    #     add2 = "Solver_ops op=scale_addscale file1=%s/testvelback.h file2=%s scale2_r=%s" % (self.path_out,tomograd,alpha)
    #     rbf1 = "%s/NONLINEAR_RBF_PHI.x rbftable=%s rbfcoord=%s verbose=1 par=%s < %s/testrbfmod.h > %s/saltoverlay.H" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,self.path_out,self.pathtemp)
    #     rbf2 = "Math file1=%s/saltoverlay.H exp='file1+1.0' | Scale > %s/saltoverlay2.H" % (self.pathtemp,self.pathtemp)
    #     add3 = "Math file1=%s/saltoverlay2.H file2=%s exp='%f*file1+file2'> %s/unclipppedvel.h" % (self.pathtemp,self.velback,float(self.vsalt),self.pathtemp)
    #     clip = "Clip clip=%f chop=greater < %s/unclipppedvel.h> %s" % (float(self.vsalt),self.pathtemp,testvelpath)
    #     rm1 = "rm %s/saltoverlay.H* %s/unclipppedvel.h %s/testrbfmod.h %s/testvelback.h" % (self.pathtemp,self.pathtemp,self.path_out,self.path_out)
    #     cmd = "%s; %s; %s; %s; %s; %s; %s; %s; %s;" % (cpp1, cpp2, add1, add2, rbf1, rbf2, add3, clip, rm1)
    #     if (self.debug):
    #         print('-----------------------------------------------------------------')
    #         print('UPDATING MODEL (linesearch')
    #         print(cmd)
    #     subprocess.call(cmd,shell=True)
    #     return



    def LINE_SEARCH(self, maxbeta, zero_objfuncval, phigrad, tomograd, dpIncrease):
        print('-----------------------------------------------------------------')
        print('LINE SEARCH')

        # Initialize the lists
        Npoints=3
        testP=[0]*Npoints
        new_objfuncval=[0]*Npoints
        X=[0]*Npoints
        Y=[0]*Npoints

        dp = maxbeta/(Npoints-1)
        ip=0
        for itry in range(0,self.Ntries):
            # Get ip value
            if (ip==0):
                testP[ip] = 0
            else:
                testP[ip] = testP[ip-1] + dp

            if (self.debug):  print(">>>>>>>>>>>>>>>>>>>>>   TEST BETA =  %s . Try #%s") % (testP[ip],itry)
            testvelpath = '%s/testvel%s_%s_%s.H' % (self.path_out,self.lsprefix,ip,itry)
            testrespath = '%s/testres%s_%s_%s.H' % (self.path_out,self.lsprefix,ip,itry)
            if(self.phiupd and self.tomoupd):
                self.UPDATE_MODEL_LS(self.debug,alpha=testP[ip],beta=testP[ip],testvelpath=testvelpath,phigrad=phigrad,tomograd=tomograd)
            if((self.phiupd) and (not self.tomoupd)):
                self.UPDATE_MODEL_LS(self.debug,alpha=0.0,beta=testP[ip],testvelpath=testvelpath,phigrad=phigrad,tomograd=tomograd)
            if((not self.phiupd) and (self.tomoupd)):
                self.UPDATE_MODEL_LS(self.debug,alpha=testP[ip],beta=0.0,testvelpath=testvelpath,phigrad=phigrad,tomograd=tomograd)

            if (ip==0):
                new_objfuncval[ip]=zero_objfuncval
                objval = "objfuncval #%s =  %s" % (ip, new_objfuncval[ip])
                print(objval)
                ip = ip+1
            else:

                # # #VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
                # #Makes random objective function values for fast testing purposes only
                # aa = random.uniform(-0.05, 0.05)
                # new_objfuncval[ip] = new_objfuncval[ip-1] + new_objfuncval[ip-1]*aa
                # # #VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

                prefixA = "syndata_LINESEARCH_%s_%s_%s" % (self.lsprefix,ip,itry)
                testsynpath = "%s/%s.H" % (self.path_out,prefixA)
                self.blockPrint()
                self.gradinstance.syn_data(self.param_reader,self.debug,prefixA,testvelpath,testsynpath) # Make the modeled data
                self.gradinstance.residual(self.param_reader,self.debug,testsynpath,testrespath) # Make the residual
                self.enablePrint()
                new_objfuncval[ip]=self.maininstance.CALC_OBJ_VAL(self.debug,testrespath) # Calc the objective function value


                # Shrink maxbeta if first objfunc val is not less than the initial objfunc val
                if (new_objfuncval[ip]>new_objfuncval[ip-1]):
                    dp = dp/2.0
                    printout1 = "objfuncval #%s =  %s (bad)" % (ip, new_objfuncval[ip])
                    print(printout1)
                    print("Shrinking the beta step size in half")
                    if (ip==1): # Reuse the last testP and objective function value since the new ones are bad
                        new_objfuncval[2] = new_objfuncval[1]
                        testP[2] = testP[1]
                    if (ip==2): # Found a place where the objective function value is increasing after a minimum
                        break
                elif (new_objfuncval[ip]==new_objfuncval[ip-1]):
                    dp = dp*dpIncrease
                    printout1 = "objfuncval #%s =  %s (bad; hasn't done anything!)" % (ip, new_objfuncval[ip])
                    print(printout1)
                    print("increasing the beta step size by %s: from dp=%s to dp=%s." % (dpIncrease,dp,dp*dpIncrease))
                else:
                    objval = "objfuncval #%s =  %s" % (ip, new_objfuncval[ip])
                    print(objval)
                    ip = ip+1

                    # Shift the results back and keep evaluating
                    if (ip>2):
                        testP[0] = testP[1]
                        testP[1] = testP[2]
                        new_objfuncval[0] = new_objfuncval[1]
                        new_objfuncval[1] = new_objfuncval[2]
                        ip = 2


        # Find the best objective function value and the Beta that made it
        bestval=max(new_objfuncval)
        maxip= (Npoints-1)
        if (self.debug): print("range(0,Npoints) = %s    maxip = %s    dp =%s       Npoints=%s") % (range(0,Npoints),maxip, dp, Npoints)
        for ip in range(0,Npoints):
            if (self.debug): print("beta = %s       objfuncval= %s   ip=%s") % (testP[ip],  new_objfuncval[ip], ip)
            if (new_objfuncval[ip]<=bestval):
                bestval=new_objfuncval[ip]
                best_ip=ip

        if (best_ip==0):
            final_beta = testP[best_ip]
            bestval=new_objfuncval[best_ip]
            print("We tried, but couldn't find a step size that was better than zero!")
            print("final_obj = %s \n") % (bestval)
        if (best_ip==2):
            final_beta = testP[best_ip]
            bestval=new_objfuncval[best_ip]
            print("The best beta was the last one we tried. You might get a better beta if you increase the Ntries parameter.")
            print("final_obj = %s \n") % (bestval)
        if (best_ip==1):
            X = testP
            Y = new_objfuncval

            print("VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV")
            # Invert the Adjoint : (X^T)-1
            a = np.array(  [ [ (X[0]**2.), X[0], 1 ], [ (X[1]**2.), X[1], 1 ], [ (X[2]**2.), X[2], 1 ] ]  )
            b = np.array(  [ [ Y[0], Y[1], Y[2] ] ]  )
            ainv = np.linalg.inv(a)

            # Print if debug
            if (self.debug): print("ainv = %s") % (ainv)
            if (self.debug): print("b = %s") % (b)

            # Multiply : ((X^T)-1)*d
            M = np.dot(ainv, np.transpose(b))
            # Find the minimum value
            final_beta = -1.0*M[1][0]/(2.*M[0][0])

            # Check the objfunc_val guess
            bestval= (final_beta**2.)*M[0][0]  +  (final_beta)*M[1][0]  +  M[2][0]
            print("final_obj (estimate) = %s \n") % (bestval)


        if (self.tomoupd and self.phiupd):
            final_beta=final_beta
            final_alpha=final_beta
        if ((self.tomoupd) and (not self.phiupd)):
            final_beta=0.0
            final_alpha=final_beta
        if ((not self.tomoupd) and (self.phiupd)):
            final_beta=final_beta
            final_alpha=0.0


        print("\nfinal_alpha = %s \n") % (final_alpha)
        print("\nfinal_beta = %s \n") % (final_beta)
        return final_alpha,final_beta,bestval

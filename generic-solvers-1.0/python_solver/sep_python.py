#module containing sep base command for file interaction
import array
import commands
import inspect
import sys,re,os,string
import struct
import math
import time
import subprocess


sepbin=os.environ["SEP"]+"/bin"
HOME=os.environ["HOME"]
open_files=dict()
debug=False #Debug flag for printing screen output of RunShellCmd as it runs commands
debug_file=None #File where debug outputs are written


def parse_args(args):
  """Function to parse command line"""
  eqs={}
  aout=[]
  eqs["basic_sep_io"]="0"
  eq=re.compile("^(.+?)=(.+)$")
  for arg in args:
    a=eq.search(arg)
    if a:
       eqs[a.group(1)]=a.group(2)
    else:
       aout.append(arg)
  return eqs,aout  
  
def get_num_axes(file):
	"""Function to obtain number of axes in a *.H file"""
	eq=re.compile("n(\d)=(\d+)")
	cmd="%s/In %s | grep \'n[0-9]=[0-9]*\' "%(sepbin,file)
	stat1,out1=commands.getstatusoutput(cmd)
	if(stat1!=0): assert False, "Error running get_num_axes system command: %s"%cmd
	axis_elements=[]
	for iaxis in out1.split('\n'):
		find = eq.search(iaxis)
		if find:
			axis_elements.append(find.group(2))
	index=[i for i,nelements in enumerate(axis_elements) if int(nelements) > 1]
	if index:
		n_axis=index[-1]+1
	else:
		n_axis=1
	return n_axis
  
def from_cmd_line(par_name,default="",conv=None):
	[pars,files]=parse_args(sys.argv)
	del pars["basic_sep_io"]
	if (par_name in pars):
		par_var = pars[par_name]
	elif(default!=""):
		par_var = default
	else:
		assert False, "Problem retrieving %s from command line"%(par_name)
	#Conversion to different format since parse_args returns string only
	if(conv=="int" and par_var!=None):
		par_var=int(par_var)
	elif(conv=="float" and par_var!=None):
		par_var=float(par_var)
	return par_var

def get_sep_axis_params(file, iaxis):
  """Note that iaxis is 1-based, returns a list of *strings* [n,o,d,label]."""
  stat1,out1=commands.getstatusoutput("%s/Get parform=no <%s n%d"%(sepbin,file,iaxis))
  stat2,out2=commands.getstatusoutput("%s/Get parform=no <%s o%d"%(sepbin,file,iaxis))
  stat3,out3=commands.getstatusoutput("%s/Get parform=no <%s d%d"%(sepbin,file,iaxis))
  stat4,out4=commands.getstatusoutput("%s/Get parform=no <%s label%d"%(sepbin,file,iaxis))
  if len(out1)==0: out1="1"
  if len(out2)==0: out2="0"
  if len(out3)==0: out3="1"
  if len(out4)==0: out4=" "
  return [out1, out2, out3, out4]
  
  
  
def sep_read_file(file):
	"""Function for reading sep files"""
	par = dict()
	suffix = ""
	get_sep_axes_params(file,par,suffix)
	stat,file_binary = commands.getstatusoutput("%s/Get parform=no <%s in"%(sepbin,file))
	f = open(file_binary,'rb')
	data = []
	for i in range(par['filesize']):
		data.append(struct.unpack('>f',f.read(4))[0])
	f.close
	ax_info=par2axinfo(par,suffix)
	return [data, ax_info]


def get_sep_axes_params(file,par,suffix):
  """par is a dictionary (both as input and as returned value) containing keys like 
  nsuffix_1, osuffix_1 etc."""
  file_size = 1
  for i in range(0,7):
    out1, out2, out3, out4 = get_sep_axis_params(file, i+1)
    if not par.has_key("n%s_%d"%(suffix,i+1)): par["n%s_%d"%(suffix,i+1)]=out1
    if not par.has_key("o%s_%d"%(suffix,i+1)): par["o%s_%d"%(suffix,i+1)]=out2
    if not par.has_key("d%s_%d"%(suffix,i+1)): par["d%s_%d"%(suffix,i+1)]=out3
    if not par.has_key("label%s_%d"%(suffix,i+1)): par["label%s_%d"%(suffix,i+1)]=out4
    file_size = file_size*int(par["n%s_%d"%(suffix,i+1)])
  par['filesize']=file_size
  stat,par['naxis']=commands.getstatusoutput("%s/In %s| grep -P 'n+[0-9]' | wc -l"%(sepbin,file))
  return par
  
def par2axinfo(par,suffix):
	"""Function to convert par dictonary to ax_info array"""
	ax_info = []
	for i in range(int(par['naxis'])):
		ax_info.append([par["n%s_%d"%(suffix,i+1)],par["o%s_%d"%(suffix,i+1)],par["d%s_%d"%(suffix,i+1)],par["label%s_%d"%(suffix,i+1)]])
	return ax_info
	

def put_sep_axis_params(file, iaxis, ax_info):
  """Note that ax_info is a list of *strings* [n,o,d,label]."""
  assert iaxis > 0
  cmd = "echo n%d=%s o%d=%s d%d=%s label%d=%s >>%s" % (iaxis,ax_info[0], iaxis,ax_info[1], iaxis,ax_info[2], iaxis,ax_info[3], file)
  RunShellCmd(cmd)
  return
  
def sep_find_datapath():
	"""Function to find SEP datapath directory"""
	stat=os.path.isfile('.datapath') 
	if stat:
		stat,out=commands.getstatusoutput("cat .datapath | head -n 1")
		datapath=out.split("=")[1]
	else:
		#check whether the local host has a datapath
		stat=os.path.isfile(HOME+"/.datapath") 
		if stat:
			stat,out=commands.getstatusoutput("cat $HOME/.datapath | grep $HOST")
			if len(out)==0:
				stat,out=commands.getstatusoutput("cat $HOME/.datapath | head -n 1")
				datapath=out.split("=")[1]
			else:
				datapath=out.split("=")[1]
		else:
			datapath=""
	return datapath
  
def sep_write_file(file,ax_info,data):
	"""Function for writing sep files"""
	datapath=sep_find_datapath()
	#write binary file
	with open(datapath+file.split('/')[-1]+'@','wb') as f:
		for b in data:
			f.write(struct.pack('>f', b))
	f.close
	#writing header/pointer file
	stat=sep_to_history(file,ax_info)
	out=RunShellCmd_output('echo in='+datapath+file.split('/')[-1]+'@'+'>>'+file)
	stat_fun=0
	return stat_fun
	
def sep_to_history(file,ax_info,ax_id=0):
	"""Function for writing sep history"""
	if (ax_id==0):
		for iaxis in range(len(ax_info)):
			put_sep_axis_params(file, iaxis+1, ax_info[iaxis])
	else:
		put_sep_axis_params(file, ax_id, ax_info)
	stat_fun = 0
	return stat_fun 
	
def sep_from_history(file):
	"""Function for reading sep history"""
	par = dict()
	suffix = ""
	get_sep_axes_params(file,par,suffix)
	ax_info=par2axinfo(par,suffix)
	return ax_info


def sep_srite(file,data):
	"""Function for writing sep files: binary only"""
	datapath=sep_find_datapath()
	#write binary file
	file_binary = datapath+file.split('/')[-1]+'@'
	if(not(open_files.has_key(file))): open_files[file]=open(file_binary,'w+b')
	for b in data:
		open_files[file].write(struct.pack('>f', b))
		open_files[file].flush()
	out=RunShellCmd_output('echo in='+file_binary+'>>'+file)
	stat_fun = 0
	return stat_fun
	
def sep_sreed(file,byte=0):
	"""Function for reading sep files: binary only
	It also opens the file and leave it open. To close call sep_close"""
	datapath=sep_find_datapath()
	par = dict()
	suffix = ""
	get_sep_axes_params(file,par,suffix)
	stat,file_binary = commands.getstatusoutput("%s/Get parform=no <%s in"%(sepbin,file))
	if(not(open_files.has_key(file))): open_files[file]=open(file_binary,'r+b')
	data = []
	if byte==0: byte=par['filesize']
	for i in range(byte):
		data.append(struct.unpack('>f',open_files[file].read(4))[0])
	return data
	
	
def sep_sseek(file,off,whence):
	"""Function to move file pointer. Same as lseek (whence: beg=0, cur=1, end=2)"""
	global open_files
	if(not(open_files.has_key(file))):
		print "%s is not open"%(file)
	else:
		open_files[file].seek(off,whence)
	stat_fun = 0
	return stat_fun
	
def sep_close(file=""):
	global open_files
	if bool(open_files):
		if file=="":
			for filename in open_files:
				if(not(open_files[filename].closed)):open_files[filename].close
				open_files=dict()
		else:
			if(open_files.has_key(file)):
				if(not(open_files[file].closed)):open_files[file].close
				open_files.pop(file)
			else:
				print "%s not open" %(file)
	return	

def write_in_file(log_file=None,info=None):
		"""Function to write log file output of a command"""
		#info: line to print in the log file
		if (log_file != None and debug):
			commands.getstatusoutput("echo \"%s\" >> %s"%(info,log_file))
		return

def wait_process(process,print_output=False):
  """Waiting a process to finish and catching stdout during process run"""
  while (process.poll() == None):
    for line in iter(process.stdout.readline,''):
      info=line.rstrip()
      print info
      if print_output:
        write_in_file(debug_file,info); print info
      process.stdout.flush()
  return
  
def RunShellCmd(cmd, print_cmd=False, print_output=False):
  if debug: 
    print_cmd=True
    print_output=True #Overwrites any previous definition
  if print_cmd:
    info = "RunShellCmd: %s"%cmd
    write_in_file(debug_file,info); print info
  process=subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
  wait_process(process,print_output)
  stat1 = process.poll()
  if stat1 != 0:
    info = "CmdError: %s" % process.stderr.read()
    write_in_file(debug_file,info); print info
    info = "Shell cmd Failed: %s!"%cmd
    write_in_file(debug_file,info); assert False, info
  return
  
#Same as RunShellCmd but returns the output
def RunShellCmd_output(cmd, print_cmd=False, print_output=False):
  if print_cmd:
    print "RunShellCmd: %s"%cmd
  stat1,out1=commands.getstatusoutput(cmd)
  if stat1 != 0:
    assert False, "Shell cmd Failed: %s!"%cmd
  if print_output:
    print "CmdOutput: %s"%out1
  return out1

#Out of core simple operations
def Dot(file1,file2,output):
	"""Dot product between two files or two file lists"""
	islist1=isinstance(file1,list)
	islist2=isinstance(file2,list)
	if(islist1 and islist2):
		if(len(file1)!=len(file2)): assert False, 'Error provide lists with same number of elements: file1=%s file2=%s'%(file1,file2)
		dot=0.0
		for  ifile,jfile in zip(file1,file2):
			cmd="Solver_ops file1=%s file2=%s op=dot |& colrm 1 11"%(ifile,jfile)
			out=RunShellCmd_output(cmd)
			dot+=float(out)
	elif(islist1 and not islist2 or not islist1 and islist2):
		assert False, 'Error provide lists or file names: file1=%s file2=%s'%(file1,file2)
	else:
		cmd="Solver_ops file1=%s file2=%s op=dot |& colrm 1 11"%(file1,file2)
		out=RunShellCmd_output(cmd)
		dot=float(out)
	cmd="Spike n1=1 d1=1 | Scale rscale=%s >%s out=%s@"%(dot,output,output)
	RunShellCmd(cmd)
	return 

def Dot_incore(file1,file2):
	"""Dot product between two files or two file lists returning a float"""
	islist1=isinstance(file1,list)
	islist2=isinstance(file2,list)
	if(islist1 and islist2):
		if(len(file1)!=len(file2)): assert False, 'Error provide lists with same number of elements: file1=%s file2=%s'%(file1,file2)
		dot=0.0
		for  ifile,jfile in zip(file1,file2):
			cmd="Solver_ops file1=%s file2=%s op=dot |& colrm 1 11"%(ifile,jfile)
			out=RunShellCmd_output(cmd)
			dot+=float(out)
	elif(islist1 and not islist2 or not islist1 and islist2):
		assert False, 'Error provide lists or file names: file1=%s file2=%s'%(file1,file2)
	else:
		cmd="Solver_ops file1=%s file2=%s op=dot |& colrm 1 11"%(file1,file2)
		out=RunShellCmd_output(cmd)
		dot=float(out)
	return dot
	
def Norm(file,output):
	"""L2 Norm of a given file or file list"""
	islist=isinstance(file,list)
	if(islist):
		norm=0.0
		for ifile in file:
			cmd="Solver_ops file1=%s op=dot |& colrm 1 11"%(ifile)
			out=RunShellCmd_output(cmd)
			norm+=float(out)	
	else:
		cmd="Solver_ops file1=%s op=dot |& colrm 1 11"%(file)
		out=RunShellCmd_output(cmd)
		norm=float(out)
	norm=math.sqrt(float(norm))
	cmd="Spike n1=1 d1=1 | Scale rscale=%s >%s out=%s@"%(norm,output,output)
	RunShellCmd(cmd)
	return 
	
def Norm_incore(file):
	"""L2 Norm of a given file or file list returning a float"""
	islist=isinstance(file,list)
	if(islist):
		norm=0.0
		for ifile in file:
			cmd="Solver_ops file1=%s op=dot |& colrm 1 11"%(ifile)
			out=RunShellCmd_output(cmd)
			norm+=float(out)	
	else:
		cmd="Solver_ops file1=%s op=dot |& colrm 1 11"%(file)
		out=RunShellCmd_output(cmd)
		norm=float(out)
	norm=math.sqrt(float(norm))
	return norm
	
def Scale(file,scalar):
	"""Multiply file or file list by a scalar"""
	islist=isinstance(file,list)
	if(islist):
		for ifile in file:
			cmd="Solver_ops file1=%s scale1_r=%s op=scale"%(ifile,scalar)
			RunShellCmd(cmd)
	else:
		cmd="Solver_ops file1=%s scale1_r=%s op=scale"%(file,scalar)
		RunShellCmd(cmd)
	return 
	
def Sum(file1,file2,scale1=1.0,scale2=1.0):
	"""Add two files or two file lists with scalar multiplication (overwrites file1)"""
	islist1=isinstance(file1,list)
	islist2=isinstance(file2,list)
	if(islist1 and islist2):
		if(len(file1)!=len(file2)): assert False, 'Error provide lists with same number of elements: file1=%s file2=%s'%(file1,file2)
		for ifile,jfile in zip(file1,file2):
			cmd="Solver_ops file1=%s scale1_r=%s file2=%s scale2_r=%s op=scale_addscale"%(ifile,scale1,jfile,scale2)
			RunShellCmd(cmd)
	elif(islist1 and not islist2 or not islist1 and islist2):
		assert False, 'Error provide lists or file names: file1=%s file2=%s'%(file1,file2)
	else:
		cmd="Solver_ops file1=%s scale1_r=%s file2=%s scale2_r=%s op=scale_addscale"%(file1,scale1,file2,scale2)
		RunShellCmd(cmd)
	return 
	
def Add(file1,file2,fileout,scale1=1.0,scale2=1.0):
	"""Add two files or two file lists with scalar multiplication and place the result in fileout"""
	islist1=isinstance(file1,list)
	islist2=isinstance(file2,list)
	islistout=isinstance(fileout,list)
	if(islist1 and islist2 and islistout):
		if(len(file1)!=len(file2)!=len(fileout)): assert False, 'Error provide lists with same number of elements: file1=%s file2=%s fileout=%s'%(file1,file2,fileout)
		for ifile,jfile,ifileout in zip(file1,file2,fileout):
			cmd="Add %s %s scale=%s,%s >%s"%(ifile,jfile,scale1,scale2,ifileout)
			RunShellCmd(cmd)
	elif(islist1 and not islist2 or not islist1 and islist2):
		assert False, 'Error provide lists or file names: file1=%s file2=%s'%(file1,file2)
	elif(islist1 and islist2 and not islistout):
		assert False, 'Error provide lists for fileout: fileout=%s'%(fileout)
	elif(not (islist1 and islist2) and islistout):
		assert False, 'Error provide only file name for fileout: fileout=%s'%(fileout)
	else:
		cmd="Add %s %s scale=%s,%s >%s"%(file1,file2,scale1,scale2,fileout)
		RunShellCmd(cmd)
	return 
	
def Zero(file):
	"""Set file or file list to zeros"""
	islist=isinstance(file,list)
	if(islist):
		for ifile in file:
			cmd="Solver_ops file1=%s op=zero"%(ifile)
			RunShellCmd(cmd)
	else:
		cmd="Solver_ops file1=%s op=zero"%(file)
		RunShellCmd(cmd)
	return 
	

def Cp(file1,file2):
	"""Copy file1 into file2"""
	islist1=isinstance(file1,list)
	islist2=isinstance(file2,list)
	if(islist1 and islist2):
		if(len(file1)!=len(file2)): assert False, 'Error provide lists with same number of elements: file1=%s file2=%s'%(file1,file2)
		for ifile,jfile in zip(file1,file2):
			#If file names are equal it won't copy the file
			if(ifile!=jfile):
				cmd="Cp %s %s"%(ifile,jfile)
				RunShellCmd(cmd)
	elif(islist1 and not islist2 or not islist1 and islist2):
		assert False, 'Error provide lists or file names: file1=%s file2=%s'%(file1,file2)
	else:
		#If file names are equal it won't copy the file
		if(file1!=file2):
			cmd="Cp %s %s"%(file1,file2)
			RunShellCmd(cmd)
	return 

def Rm(file):
	"""Remove file"""
	islist=isinstance(file,list)
	if(islist):
		for ifile in file:
# 			cmd="Rm %s"%(ifile)
# 			RunShellCmd(cmd)
			rm_sep_file(ifile)
	else:
# 		cmd="Rm %s"%(file)
# 		RunShellCmd(cmd)
		rm_sep_file(file)
	return 

def rm_sep_file(file):
	cmd1="rm -f `Get <%s in parform=n`"%(file)
	cmd2="rm -f %s"%(file)
	RunShellCmd(cmd1)
	RunShellCmd(cmd2)
	return

def tmp_file(file):
	"""Function to return a file name with current time appended"""
	islist=isinstance(file,list)
	if(islist): assert False, "Function tmp_file does not support file list: %s"%(file)
	datapath=sep_find_datapath()
	current_time=str(int(time.time()*1000000))
	file_name=datapath+file+current_time
	return file_name
	
def Get_value(file,nelements=1):
	"""Function to obtain file values"""
	islist=isinstance(file,list)
	if(islist): assert False, "Function Get_value does not support file list: %s"%(file)
	cmd="Disfil < %s number=n count=%s col=1 type=f format=%%50.45e "%(file,nelements)
	out=RunShellCmd_output(cmd)
	if nelements==1:
		value=float(out)
	else:
		value=[]
		value=[0]*nelements
		outsplit=out.split("\n")
		for ii in range(0,nelements):
			value[ii]=float(outsplit[ii])
	return value
	
	
	
	
	
	

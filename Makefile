

################################################################################

default: EXE NR ER CR

################################################################################

EXE:
	make --directory=first-order-updating EXE
	make --directory=hessian-updating EXE

ER:
	make --directory=first-order-updating ER
	make --directory=hessian-updating ER
	cp first-order-updating/Fig/*.pdf Fig/.
	cp hessian-updating/Fig/*.pdf Fig/.

CR:
	make --directory=first-order-updating CR
	make --directory=hessian-updating CR
	cp first-order-updating/Fig/*.pdf Fig/.
	cp hessian-updating/Fig/*.pdf Fig/.

NR:
	make --directory=first-order-updating NR
	make --directory=hessian-updating NR
	cp first-order-updating/Fig/*.pdf Fig/.
	cp hessian-updating/Fig/*.pdf Fig/.

################################################################################

burn:  
	make --directory=first-order-updating burn
	make --directory=hessian-updating burn

################################################################################

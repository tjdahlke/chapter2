#!/usr/bin/python
import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess

from batch_task_executor import *
import pbs_util

#=======================================================================================================
#=======================================================================================================
#=======================================================================================================


class HESSGN_OP(BatchTaskComposer):
    def __init__(self, param_reader):
        BatchTaskComposer.__init__(self)

        # Read in the job parameters
        self.dict_args = param_reader.dict_args
        self.T_BIN = self.dict_args['T_BIN']
        self.wavelet = self.dict_args['wavelet']
        self.nshots = int(self.dict_args['nfiles'])
        self.nsh_perjob = int(self.dict_args['nfiles_perjob'])
        self.ish_beg = int(self.dict_args['ish_beg'])
        self.path_out = param_reader.path_out
        self.vel_path = self.dict_args['vel_path']
        self.hess_in_path = self.dict_args['hess_in_path']
        self.genpar = self.dict_args['genpar']
        self.fullsrcwave_name = self.dict_args['fullsrcwave_name']
        self.fullrecwave_name = self.dict_args['fullrecwave_name']
        self.wavelet = self.dict_args['wavelet']
        self.res_path = self.dict_args['res_path']
        self.maskvelb = self.dict_args['maskvelb']
        self.masksalt = self.dict_args['masksalt']
        self.scaling = self.dict_args['scaling']
        self.inversionname = self.dict_args['inversionname']

        param_reader.prefix=self.inversionname

        # Create all subjobs info
        self.njobs = int(math.ceil(self.nshots / float(self.nsh_perjob)))
        self.subjobids = range(0, self.njobs)
        self.subjobfns_list = [None] * self.njobs
        # List of actual shot number (starting @ 0)
        self.subjobIND_list = range(
            self.ish_beg, self.nshots + 1, self.nsh_perjob)

        # Make a list of all the files for each job ( shots per job)
        for i in range(0, self.njobs):
            ish_start = self.subjobIND_list[i]
            nsh = min(self.nsh_perjob, self.nshots - ish_start)
            self.subjobfns_list[i] = [None] * nsh
            for j in range(0, nsh):
                self.subjobfns_list[i][j] = '%s/%s-%s.H' % (
                    self.path_out, self.inversionname, ish_start + j + 1)
        return

    def GetSubjobScripts(self, subjobid):
        # Return the scripts content for that subjob.
        subjobfns = self.subjobfns_list[subjobid]
        # Build scripts that create the files designated in subjobfns
        scripts = []
        cnt = 0
        nf_act = len(subjobfns)  # number of files

        for fn in subjobfns:
            shot = self.nsh_perjob * subjobid + cnt + 1
            cnt += 1
            if cnt > nf_act:
                break
            selectres = "Window3d n3=1 f3=%s < %s > %s/singleres-%s.H" % (
                int(shot - 1), self.res_path, self.path_out, shot)
            inOP = "Cp %s %s/fullmodGNin%s_%s.H" % (self.hess_in_path, self.path_out, shot, self.inversionname)
            operator = "%s/%s verbose=1 adj=0" % (self.T_BIN, 'HESSIAN_GN.x')
            inputs1 = 'par=%s isou=%s  data=%s/singleres-%s.H' % (
                self.genpar, shot, self.path_out, shot)
            inputs2 = 'wavelet=%s velmod=%s srcwave_in=%s/%s_%s.H ' % (
                self.wavelet, self.vel_path, self.path_out, self.fullsrcwave_name, shot)
            outputs = '< %s/fullmodGNin%s_%s.H > %s/fullmodGNout%s_%s.H  gncomp=%s/gncomp%s_%s.h  wemvacomp=%s/wemvacomp%s_%s.h' % (
                self.path_out, shot, self.inversionname, self.path_out, shot, self.inversionname, self.path_out, shot, self.inversionname, self.path_out, shot, self.inversionname)
            outOP = "Cp %s/fullmodGNout%s_%s.H %s" % ( self.path_out, shot, self.inversionname, fn)
            # Note: need the \n at the end... also cant use < in string itself.
            cmd = '%s; %s; %s %s %s %s; %s;\n' % (
                selectres, inOP, operator, inputs1, inputs2, outputs, outOP)
            scripts.append(cmd)
        return scripts, str(subjobid)

    def GetSubjobsInfo(self):
        '''Should return two lists, subjobids and subjobfns.'''
        return copy.deepcopy(self.subjobids), copy.deepcopy(self.subjobfns_list)


class make_hessGN(object):
    def __init__(self, param_reader):

        #-------------------------------------------------------------------------------------
        dict_args = param_reader.dict_args
        self.hess_in_path = dict_args['hess_in_path']
        self.hess_out_path = dict_args['hess_out_path']
        self.path_out = param_reader.path_out
        fn_seph_list = ' '

        #----- (PLAIN) GN HESSIAN APPLIED TO RTM GRADIENT --------------------------------------------
        job5 = HESSGN_OP(param_reader)
        bte = BatchTaskExecutor(param_reader)
        prefix = param_reader.prefix
        print "--------- Submitting/Running GN HESSIAN (PLAIN) ---------------------------"
        bte.LaunchBatchTask(prefix, job5)

        #------ Combine the files to one single file -----------------------------------------
        _, fns_list = job5.GetSubjobsInfo()
        fn_seph_list = [fn for fns in fns_list for fn in fns]
        args = ''
        for i in range(0, len(fn_seph_list)):
            args = '%s %s' % (args, fn_seph_list[i])
        cmd = 'Cat %s | Transp plane=23 | Stack maxsize=1000 > %s out=%s@ ' % (
            args, self.hess_out_path, self.hess_out_path)
        # Add only does like 5 files!!!! DONT USE Add!!
        print "--------- Stacking GN HESSIAN (PLAIN) files  ------------------------------"
        print(cmd)
        subprocess.call(cmd, shell=True)


        #----- Removing ADJOINT and FORWARD GN HESSIAN output files from wrk directory -------------
        ars_hess = ''
        for i in range(0, len(fn_seph_list)):
            ars_hess = '%s %s' % (ars_hess, fn_seph_list[i])
        cmd1 = 'rm -rf %s' % (ars_hess)
        cmd2 = 'rm -rf %s/tmp*' % (self.path_out)
        cmd3 = 'rm -rf %s/singleres*' % (self.path_out)
        cmd = '%s; %s; %s;' % (cmd1, cmd2, cmd3)
        subprocess.call(cmd, shell=True)
        print "--------- Removing ADJOINT and FORWARD GN HESSIAN (PLAIN) output files from wrk directory  ------------------------------"
        print(cmd)

        return


if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------- APPLY GAUSS NEWTON HESSIAN TO GRADIENT ----------------
    start_time = time.time()
    make_hessGN(param_reader)
    elapsed_time = (time.time() - start_time) / 60.0
    print("ELAPSED TIME FOR BTE Hessian (min) : ")
    print(elapsed_time)

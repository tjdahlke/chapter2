#!/usr/local/bin/python

import subprocess
import sys
import os
import sepbase
from pbs_util import JobParamReader
from subprocess import Popen, PIPE


class JobParamReader:
    '''Parse the common parameters'''

    def __init__(self, dict_args):
        self.dict_args = dict_args
        return


class RBF_INVERSION(object):
    def __init__(self, param_reader):
        #----------------------------------------------------------------
        self.dict_args = param_reader.dict_args
        self.pathtemprbf = self.dict_args['pathtemprbf']
        self.gensolve_NLF = "%s/NLforward_RBF.txt" % (self.pathtemprbf)
        self.gensolve_F = "%s/forward_RBF.txt" % (self.pathtemprbf)
        self.gensolve_A = "%s/adjoint_RBF.txt" % (self.pathtemprbf)
        self.solverpath = self.dict_args['solverpath']
        self.T_BIN = self.dict_args['T_BIN']
        self.genpar = self.dict_args['genpar']
        self.rbfiter = self.dict_args['rbfiter']
        self.salt_match = self.dict_args['salt_match']
        self.rbf_path = self.dict_args['rbf_path']
        self.masksalt = self.dict_args['masksalt']
        self.rbfcoord = self.dict_args['rbfcoord']
        self.rbftable = self.dict_args['rbftable']
        self.inversionname = self.dict_args['inversionname']
        self.phiguide = self.dict_args['phiguide']
        return

    def RBF_solver(self, param_reader, debug):

        # -------------------------------------------------------------------------------------
        # Write the NON-LINEAR FORWARD .txt file
        text_file = open(self.gensolve_NLF, "w")
        cmdNLF = "RUN: %s/NONLINEAR_RBF_PHI.x rbftable=%s rbfcoord=%s verbose=1 par=%s < input.H > output.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar)
        text_file.write("%s" % cmdNLF)
        text_file.close()

        # -------------------------------------------------------------------------------------
        # Write the LINEARIZED FORWARD .txt file
        text_file = open(self.gensolve_F, "w")
        cmdF = "RUN: %s/LINEARIZED_RBF.x rbftable=%s rbfcoord=%s masksaltout=masksaltout.h verbose=1 adjoint=0 par=%s < input.H > output.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar)
        text_file.write("%s" % cmdF)
        text_file.close()

        # -------------------------------------------------------------------------------------
        # Write the LINEARIZED ADJOINT .txt file
        text_file = open(self.gensolve_A, "w")
        cmdA = "RUN: %s/LINEARIZED_RBF.x rbftable=%s rbfcoord=%s masksaltout=masksaltout.h verbose=1 adjoint=1 par=%s < input.H > output.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar)
        text_file.write("%s" % cmdA)
        text_file.close()

        if (debug):
            print('-----------------------------------------------------------------')
            print(cmdNLF)
            print('-----------------------------------------------------------------')
            print(cmdF)
            print('-----------------------------------------------------------------')
            print(cmdA)

        # Make a smart guess initial model
        cmd = "%s/APPLY_RBF.x rbftable=%s rbfcoord=%s verbose=1 adjoint=1 par=%s < %s > %s/initmodel.H" % (
            self.T_BIN, self.rbftable, self.rbfcoord, self.genpar, self.salt_match, self.pathtemprbf)
        if (debug):
            print('-----------------------------------------------------------------')
            print " Making an initial model (guess based on initial phi)"
            print(cmd)
        subprocess.call(cmd, shell=True)
        sys.stdout.flush()

        # Run the generic solver with those files
        epsilon=0.0418280965092
        if(not os.path.isfile(self.rbf_path)):
            cmd1 = "%s/python_solver/generic_non_linear_prob.py epsilon=%s epsilon_scale=no ref_model=%s fwd_nl_cmd_file=%s  fwd_cmd_file=%s  adj_cmd_file=%s iteration_movies=obj,model,gradient,residual " % (
                self.solverpath, epsilon, self.phiguide, self.gensolve_NLF, self.gensolve_F, self.gensolve_A)
            cmd2 = "debug=no data=%s init_model=%s/initmodel.H niter=%s inv_model=%s suffix=%s dotprod=0 stepper=parabolic tolobjrel=0.0005" % (
                self.salt_match, self.pathtemprbf, self.rbfiter, self.rbf_path,self.inversionname)
            cmd = "%s %s" % (cmd1, cmd2)
            if (debug):
                print(
                    '-----------------------------------------------------------------')
                print " LAUNCHING GENERIC NON-LINEAR PYTHON SOLVER FOR RBF FITTING"
                print(cmd)
                sys.stdout.flush()
            subprocess.call(cmd, shell=True)
        else:
            print "------------------------------------------------------------"
            print "Inverted result already exists: Skipping RBF fitting inversion"
        sys.stdout.flush()
        return


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = JobParamReader(eq_args_from_cmdline)

    print("=================================================================\n")
    debug = True
    # INITIALIZING THE MAIN FUNCTION SPACE and RBF SOLVER
    mainRBF = RBF_INVERSION(param_reader)
    mainRBF.RBF_solver(param_reader, debug)

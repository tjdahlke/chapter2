#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from main_functions import *
from inversion import *
from gradmaker import *
from linesearchRBF_adaptive import *


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    levelset_iter = int(dict_args['levelset_iter'])
    dpIncrease=1.25
    Ntries = 15
    debug = True
    LStomoupd=False
    LSphiupd=True

    inversionname = param_reader.dict_args['inversionname']
    vel_path = param_reader.dict_args['vel_path']
    syn_path = param_reader.dict_args['syn_path']
    res_path = param_reader.dict_args['res_path']
    rtm_path = param_reader.dict_args['rtm_path']
    phi_path = param_reader.dict_args['phi_path']
    masksalt = param_reader.dict_args['masksalt']
    maskvelb = param_reader.dict_args['maskvelb']
    pathtemp = param_reader.dict_args['pathtemp']
    sparsemodel = param_reader.dict_args['sparsemodel']

    phigrad = param_reader.dict_args['phigrad']
    tomograd = param_reader.dict_args['tomograd']
    pathtemphess = param_reader.dict_args['pathtemphess']
    gnhess = int(param_reader.dict_args['gnhess'])
    hess_out_path = param_reader.dict_args['hess_out_path']

    # INITIALIZING THE MAIN FUNCTION SPACES. FUNCTIONS THAT CALL OTHER FUNCTIONS RELY ON THESE OBJECT INSTANCES
    main = MAIN_FUNCTIONS(param_reader)
    gradient_maker = GRADMAKER(param_reader)
    mainLS = MAIN_LS_FUNCTIONS(debug, param_reader, gradient_maker, main, inversionname, Ntries, LStomoupd, LSphiupd)
    inversion = inversion(param_reader,inversionname)

    ###################################################################################################
    #----------- BEGIN LEVELSET ITERATIONS ------------------------------------------------------------
    for i in range(0, levelset_iter):
        print("###################################################################################################")
        print("########################      LEVELSET ITERATION   %s / %s   ######################################") % (i, (levelset_iter - 1))
        print("###################################################################################################")

        #------- MAKE SYNTHETIC DATA AND RESIDUAL -------------------------------------------------
        gradient_maker.syn_data(param_reader, debug,"syndata_MAIN", vel_path, syn_path)
        gradient_maker.residual(param_reader, debug, syn_path, res_path)

        #-------- CALCULATE THE OBJECTIVE FUNCTION VALUE  ---------------------------------------------
        new_objfuncval = main.CALC_OBJ_VAL(debug, res_path)
        print(">>>>>>>>>>>> Objective function value: %s" % (new_objfuncval))

        print(" ====================================================================================")
        #------- MAKE FULL RTM GRADIENT ------------------------------------------
        gradtag = 'primary_gradient'
        gradient_maker.born_full(param_reader, debug, gradtag, res_path, rtm_path)

        #------- MAKE MASKS FOR D OPERATOR ---------------------------------------
        main.MAKE_MASKS_RBF(debug,phi_path,masksalt,maskvelb)

        #------- MAKE SCALING ----------------------------------------------------
        main.MAKE_SCALING(debug)

        #----- Calc phi_grad & b_grad ------------------------------------------
        main.DELTAP_BEFORE_RBF1(debug,rtm_path,sparsemodel)

        hess_out_path=sparsemodel;

        #----- Calc phi_grad & b_grad ------------------------------------------
        main.DELTAP_AFTER_RBF1(debug,hess_out_path,phigrad,tomograd)

        print(" ====================================================================================")
        #----- Calc maxbeta ----------------------------------------------------
        maxbeta = main.CALC_MAXBETA_RBF(debug,0.25,phigrad,phi_path)
        # maxbeta = main.CALC_MAXBETA_RBF(debug,1.0,phigrad,phi_path)

        #------- LINE SEARCH ---------------------------------------------------
        final_alpha,final_beta,final_obj=mainLS.LINE_SEARCH(maxbeta, new_objfuncval, phigrad, tomograd, dpIncrease)

        #------- SAVE THE OUTPUTS FROM THIS ITERATION  -------------------------
        main.SAVE_INTERMEDIATE_FILES(debug,inversionname)
        main.SAVE_INTERMEDIATE_VALUES(debug, new_objfuncval, maxbeta, final_alpha, final_beta)

        #------- BUILD MODEL FROM NEW PHI AND MAXBETA  -------------------------
        newphi_path="%s_GUESS_%s" % (phi_path,i)
        mainLS.UPDATE_MODEL_LS(debug, final_alpha, final_beta, phigrad, tomograd, vel_path, newphi_path)
        main.COPY(debug,newphi_path,phi_path)

        #-------- CLEAN THE WORKING DIRECTORY   --------------------------------
        main.CLEAN_UP(debug,inversionname)

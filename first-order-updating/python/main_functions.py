import subprocess
import numpy as np
import shlex
import os.path
import commands



class MAIN_FUNCTIONS(object):
    def __init__(self, param_reader):
        dict_args = param_reader.dict_args
        self.pathtemp = dict_args['pathtemp']
        self.path_out = dict_args['path_out']
        self.rtm_path= dict_args['rtm_path']
        self.res_path = dict_args['res_path']
        self.T_BIN = dict_args['T_BIN']
        self.vel_path = dict_args['vel_path']
        self.phigrad = dict_args['phigrad']
        self.tomograd = dict_args['tomograd']
        self.genpar = dict_args['genpar']
        self.scaling = dict_args['scaling']

        self.phi_path = dict_args['phi_path']
        self.velback = dict_args['velback']
        self.phi_path = dict_args['phi_path']
        self.rbf_path = dict_args['rbf_path']
        self.hess_out_path = dict_args['hess_out_path']

        self.masksalt = dict_args['masksalt']
        self.maskvelb = dict_args['maskvelb']
        self.sparsemodel = dict_args['sparsemodel']
        self.vsalt = dict_args['vsalt']
        self.rbfcoord = dict_args['rbfcoord']
        self.rbftable = dict_args['rbftable']
        self.guide = dict_args['guide']

        return



    def MAKE_SCALING(self, debug):
        #------- MAKE SCALING ------------------------------------------
        cmd="Math file1=%s exp='%s-file1'> %s" % (self.velback,float(self.vsalt),self.scaling)
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE SCALING')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def COPY(self, debug, fileIN, fileOUT):
        #----- COPY A FILE  -------------------
        cmd1 = "Cp %s %s " % (fileIN,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('COPY FILE')
            print(cmd1)
        return


    def HADAMARD_PRODUCT(self, debug, file1, file2, fileOUT):
        #----- HADAMARD_PRODUCT TWO FILES  -------------------
        cmd1 = "Math_base file1=%s file2=%s exp='file1*file2' > %s " % (file1,file2,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('HADAMARD_PRODUCT OF TWO FILES (ELEMENT WISE MULTIPLICATION)')
            print(cmd1)
        return



    def SCALE(self, debug, fileIN, fileOUT, coeff):
        #----- SCALE A FILE  -------------------
        cmd1 = "Math_base file1=%s exp='file1*%s'> %s " % (fileIN,coeff,fileOUT)
        aa=commands.getstatusoutput(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('SCALE FILE')
            print(cmd1)
        return



    def MAKE_MASKS_RBF(self, debug, phi_path, masksalt, maskvelb, **optparams):
        if ('smoothrad' in optparams):
            smoothrad = optparams['smoothrad']
        else:
            smoothrad =' '
        if ('smoothpass' in optparams):
            smoothpass = optparams['smoothpass']
        else:
            smoothpass =' '
        #------- MAKE VELB and SALT MASKS ------------------------------------------
        cmd1="%s/MAKE_MASK_RBF.x guide=%s par=%s smoothrad=%s smoothpass=%s < %s maskvelb=%s masksalt=%s maskvelbfull=maskvelbfull.h >/dev/null" % (self.T_BIN,self.guide,self.genpar,smoothrad,smoothpass,phi_path,maskvelb,masksalt)
        # cmd2="Math file1=%s exp='1.0-file1' | Scale > guide.tmp" % (self.guide)
        # cmd3="Add scale=1,1 masksalt.tmp guide.tmp > sum.tmp"
        # cmd4="%s/CLIP.x lessthan=no clipval=1.0 verbose=1 replaceval=1.0 < sum.tmp | Scale > %s; rm masksalt.tmp guide.tmp sum.tmp" % (self.T_BIN,masksalt)
        # cmd="%s; %s; %s; %s" % (cmd1, cmd2, cmd3, cmd4)
        cmd=cmd1;
        if (debug):
            print('-----------------------------------------------------------------')
            print('MAKE SALT & VELB MASK')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


 # For sparsemodel that includes ONLY phi updates
    def DELTAP_BEFORE_RBF1(self, debug, rtm_path, sparsemodel):
        #------- MAKE PHI & TOMO GRADIENTS  ------------------------------------
        cmd1 = "%s/APPLY_Dop_RBF1.x rbftable=%s rbfcoord=%s masksalt=%s scaling=%s verbose=0 adjoint=1 par=%s < %s > %s" % (self.T_BIN,self.rbftable,self.rbfcoord,self.masksalt,self.scaling,self.genpar,rtm_path,sparsemodel)
        #------- PURELY FOR VISUALIZATON PURPOSES ------------------------------
        cmd2 = "%s/APPLY_RBF.x rbftable=%s rbfcoord=%s verbose=0 adjoint=0 par=%s < %s > %s/fullphigrad.H" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,self.sparsemodel,self.pathtemp)
        cmd = "%s; %s" % (cmd1, cmd2)
        if (debug):
            print('-------------------------------------------------------------')
            print('MAKE PHIrbf GRADIENTS')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def DELTAP_AFTER_RBF1(self, debug, sparseinput, phigrad, tomograd):
        #------- MAKE SCALED RBF GRADIENT TO USE IN LINESEACH  ------------------------------------------
        cmd1 = "Scale < %s > %s_scaled" % (sparseinput, sparseinput)
        #------- CONVERT RBF GRADIENT TO VEL MODEL SPACE FOR VISUALIZING  ------------------------------------------
        cmd2 = "%s/APPLY_RBF.x rbftable=%s rbfcoord=%s verbose=0 adjoint=0 par=%s < %s_scaled > %s" % (self.T_BIN,self.rbftable,self.rbfcoord,self.genpar,sparseinput,phigrad)
        cmd3 = "Cp %s %s; Solver_ops op=zero file1=%s" % (phigrad,tomograd,tomograd)
        cmd4 = "rm %s_scaled" % (sparseinput)
        cmd = "%s; %s; %s;" % (cmd1, cmd2, cmd3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Make phigrad (from RBF model after inversion) and EMPTY tomograd')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def CALC_MAXBETA_RBF(self, debug, upperlim, phigrad, phi_path):
        # --------  Calculate the maxbeta   ----------------------
        operator = '%s/MAXBETA_RBF.x upperlim=%s' % (self.T_BIN,upperlim)
        inputs1 = 'phigrad=%s phi=%s par=%s'  % (phigrad,phi_path,self.genpar)
        cmd = '%s %s' % (operator, inputs1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('CALC_MAXBETA')
            print(cmd)
        subprocess.call(cmd,shell=True)
        # --------  Grab the maxbeta   ----------------------
        file = open('maxbetaval.txt')
        for line in file:
            maxbeta = float(line)
        if (debug):
            print('MAXBETA = %s') % (maxbeta)
        return maxbeta



    def SAVE_INTERMEDIATE_FILES(self, debug, invname):
        cmd1 = "%s %s %s %s %s %s %s %s %s" % (self.masksalt,self.rtm_path,self.vel_path,self.phigrad,self.hess_out_path,self.phi_path,self.tomograd,self.scaling,self.rbf_path)
        qq=shlex.split(cmd1)
        if (debug):
            print('-----------------------------------------------------------------')
            print('SAVING INTERMEDIATE FILES')
        for file in qq:
            filecomp = '%scomp' % (file)
            if (os.path.isfile(filecomp)):
                cmd1a = 'Cat %s  %s > %s/out.H' % (filecomp, file, self.path_out)
                cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
                cmd= '%s; %s' % (cmd1a, cmd1b)
            else:
                cmd = 'Cp %s %s ' % (file, filecomp)
            cmdp = '%s\n' % (cmd)
            if (debug):
                print(cmdp)
            subprocess.call(cmd,shell=True)

        # Saving the residuals
        filecomp = '%scomp' % (self.res_path)
        if (os.path.isfile(filecomp)):
            cmd1a = 'Cat %s  %s > %s/out.H' % (filecomp, self.res_path, self.path_out)
            cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
            cmd= '%s; %s' % (cmd1a, cmd1b)
        else:
            cmd = 'Cp %s %s ' % (self.res_path, filecomp)
        if (debug):
            print(cmd)
        subprocess.call(cmd,shell=True)

        # Saving the hessian inverison stuff
        prefs = "%s %s %s" % ("obj","model","gradient")
        qq=shlex.split(prefs)
        for pref in qq:
            file = '%s%s.H' % (pref,invname)
            filecomp = '%s%s.Hcomp' % (pref,invname)
            if (os.path.isfile(filecomp)):
                cmd1a = 'Cat %s  %s > %s/out.H' % (filecomp, file, self.path_out)
                cmd1b = 'rm %s ; mv %s/out.H %s' % (filecomp,self.path_out,filecomp)
                cmd= '%s; %s' % (cmd1a, cmd1b)
            else:
                cmd = 'Cp %s %s ' % (file, filecomp)
            cmdp = '%s\n' % (cmd)
            if (debug):
                print(cmdp)
            subprocess.call(cmd,shell=True)
        return


    def SAVE_INTERMEDIATE_VALUES(self, debug, objfuncval, maxbeta, final_alpha, final_beta):

        if (debug):
            print('-----------------------------------------------------------------')
            print('SAVING INTERMEDIATE VALUES')

        file1="%s/objfuncval.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file1,objfuncval,file1)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file2="%s/maxbeta.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file2,maxbeta,file2)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file3="%s/final_alpha.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file3,final_alpha,file3)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)

        file4="%s/final_beta.txt" % (self.pathtemp)
        cmd = "touch %s; echo '%s' >> %s" % (file4,final_beta,file4)
        if (debug): print(cmd)
        subprocess.call(cmd,shell=True)
        return


    def CALC_OBJ_VAL(self, debug, res_path):
        #----- CALCULATE THE OBJECTIVE FUNCTION VALUE  -------------------
        cmd1 = "Solver_ops op=dot file1=%s file2=%s" % (res_path,res_path)
        aa=commands.getstatusoutput(cmd1)
        new_objfuncval=float(aa[1].split()[2])
        return new_objfuncval



    def CLEAN_UP(self, debug, invname):
        #----- Write the velmodel to output (current iteration) --------------------
        cmd1 = 'rm -f %s/*' % (self.path_out)
        cmd2 = 'rm -f %s' % (self.hess_out_path)
        cmd3 = 'rm -f %s' % (invname)
        cmd =  "%s; %s; %s;" % (cmd1, cmd2, cmd3)
        if (debug):
            print('-----------------------------------------------------------------')
            print('Remove WRK and Inversion output')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return



    def CLEAN_TMP(self, debug):
        #----- Write the velmodel to output (current iteration) --------------------
        cmd = 'rm %s/*' % (self.path_tmp)
        if (debug):
            print('-----------------------------------------------------------------')
            print('CLEAN_TMP')
            print(cmd)
        subprocess.call(cmd,shell=True)
        return

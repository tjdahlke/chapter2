#!/bin/tcsh
### Each node on cees-rcf is Quad CPU with Quad core (16 cores).
#PBS -N jobname
#PBS -l nodes=1:ppn=16
#PBS -q sep
#PBS -V
#PBS -j oe
#PBS -e /data/sep/tjdahlke/research/wrk
#PBS -o /data/sep/tjdahlke/research/wrk
#PBS -d /data/sep/tjdahlke/research/wrk
#PBS -l walltime=00:100:00

cd $PBS_O_WORKDIR
setenv OMP_NUM_THREADS 16
echo $OMP_NUM_THREADS

echo WORKDIR is: $PBS_O_WORKDIR
sleep 1 #5
echo NodeFile is: $PBS_NODEFILE
echo hostname is: $HOSTNAME



!===============================================================================
!		Linearized Derivative of the Heaviside applied to a Radial Basis function
!
!		d/dz(H(RBF(z)))
!
!		MODEL:  Nrbf
!		DATA:  	Nz x Nx
!===============================================================================

program LINEARIZED_RBF
	use sep
	use rbf_mod
	use drlse_mod

	implicit none
	integer								:: nrbf, nz, nx, trad
	real								:: timer_sum=0.
	real, dimension(:,:), allocatable	:: data, bin, binX, binZ, masksalt, rbfcoord, rbftable
	real, dimension(:), allocatable		:: model
	logical								:: verbose, adjoint
	integer, dimension(8)               :: timer0, timer1

	call sep_init()
	call DATE_AND_TIME(values=timer0)

	if(verbose) write(0,*) "================ Read in initial parameters =================="
	call from_param("n1", nz)
	call from_param("n2", nx)
	call from_param("verbose", verbose, .TRUE.)
	call from_param("adjoint", adjoint, .FALSE.)
	call from_param("trad", trad, 15)
	call from_aux("rbfcoord", "n1", nrbf)

	allocate(rbftable((2*trad+1), (2*trad+1)))
	allocate(masksalt(nz, nx))
	allocate(model(nrbf))
	allocate(data(nz, nx))
	allocate(bin(nz, nx))
	allocate(binX(nz, nx))
	allocate(binZ(nz, nx))
	allocate(rbfcoord(nrbf,2))

	call sep_read(rbfcoord,"rbfcoord")
	call sep_read(rbftable,"rbftable")


	if (adjoint) then
		if (verbose) write(0,*) "================ Reading in RBF surface perturbation ================"
		call sep_read(data)
		data=2*data ! To create a implicit surface that is +1 in salt and -1 outside salt
		bin=data
	else
		if (verbose) write(0,*) "==================== Reading in RBF weights perturbation ================="
		call sep_read(model)
		model=2*model ! To create a implicit surface that is +1 in salt and -1 outside salt
		call Hrbf(model,bin)
	endif

	call drlse_init(nz, nx, 1.0, 1.0)
	call gradientR(bin, binX, binZ)
	masksalt = SQRT(binX**2+binZ**2)
	WHERE (masksalt >0.0) masksalt = 1.0

	if (exist_file('masksaltout')) then
		call to_history("n1"    ,nz					,'masksaltout')
		call to_history("n2"    ,nx					,'masksaltout')
		call to_history("label1",'Sparse values'	,'masksaltout')
		call sep_write(masksalt,"masksaltout")
	end if

!##########################################################################################
	if(verbose) write(0,*) "========= Initialize the submodules ====================================="
	call rbf_init(nz, nx, nrbf, trad, rbfcoord, rbftable)
	call rbf_init_mask(masksalt)
	if(verbose) write(0,*) "========= Running main operator ====================================="
	call L_Hrbf(adjoint,.False.,model,data)

!##########################################################################################

	if (adjoint) then
		if(verbose) write(0,*) "========= Write out RBF file (weights perturbation) ================="
		call to_history("n1",nrbf)
		call to_history("n2",1)
		call to_history("label1",'# rbf centers')
		call sep_write(model)
	else
		if(verbose) write(0,*) "========= Write out RBF file (surface perturbation) ================="
		call to_history("n1",nz)
		call to_history("n2",nx)
		call to_history("label1",'z [km]')
		call to_history("label2",'x [km]')
		call sep_write(data)
	end if

	!-----------------------------------------------------------------------------
	call DATE_AND_TIME(values=timer1)
	timer0 = timer1-timer0
	timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
	write(0,*) "timer sum (sec): ", timer_sum/1000
	deallocate(data,model)
	if (verbose) write(0,*) "LINEARIZED_RBF program complete"

end program

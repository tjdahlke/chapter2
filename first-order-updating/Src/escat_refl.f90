module escat_refl_mod

  implicit none

  integer, private                         :: n1, n2, nt, nh, oh, it, ih, h, ix, iz
  real, dimension(:,:,:), pointer, private :: refl

  contains

  subroutine escat_refl_init(n1_in, n2_in, nt_in, nh_in, oh_in, refl_in)
    integer                        :: n1_in, n2_in, nt_in, nh_in, oh_in
    real, dimension(:,:,:), target :: refl_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    nh = nh_in
    oh = oh_in
    refl => refl_in
  end subroutine

  function escat_refl_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call escat_refl_op2(adj, add, model, data)
    stat=0
  end function

  subroutine escat_refl_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2,nt)   :: model
    real, dimension(n1,n2,nt) :: data
    if(adj) then
      if(.not. add) model = 0.
      do it=1,nt
        do ih=1,nh
          h = ih - 1 + oh
          do ix=1+abs(h)-h,n2-abs(h)-h
            model(:,ix,it) = model(:,ix,it) + refl(:,ix+h,ih)*data(:,ix+2*h,it)
          end do
        end do
      end do
    else
      if(.not. add) data = 0.
      do it=1,nt
        do ih=1,nh
          h = ih - 1 + oh
          do ix=1+abs(h)+h,n2-abs(h)+h
            data(:,ix,it) = data(:,ix,it) + refl(:,ix-h,ih)*model(:,ix-2*h,it)
          end do
        end do
      end do
    end if
  end subroutine

end module

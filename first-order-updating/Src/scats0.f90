module scats0_mod

  implicit none

  integer, private                         :: n1, n2, nt, it, ix, iz
  real, dimension(:,:,:), pointer, private :: wflds0

  contains

  subroutine scats0_init(n1_in, n2_in, nt_in, wflds0_in)
    integer                        :: n1_in, n2_in, nt_in
    real, dimension(:,:,:), target :: wflds0_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    wflds0 => wflds0_in
  end subroutine

  function scats0_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call scats0_op2(adj, add, model, data)
    stat=0
  end function

  subroutine scats0_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2)      :: model
    real, dimension(n1,n2,nt) :: data
    if(adj) then
      if(.not. add) model = 0.
      do it=1,nt
        model = model + data(:,:,it)*wflds0(:,:,it)
      end do
    else
      if(.not. add) data = 0.
      do it=1,nt
        data(:,:,it) = data(:,:,it) + model*wflds0(:,:,it)
      end do
    end if
  end subroutine

end module

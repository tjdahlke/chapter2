
!===================================================================================
!		Assemble the velocity model from the implicit surface and background vel
!===================================================================================

program MAKE_VEL_MODEL
use sep
implicit none

	integer										:: nz, nx, ny
	real										:: oz,ox,oy,dz,dx,dy
	logical										:: verbose
	real										:: vsalt
	real, dimension(:,:), allocatable			:: phi2d, velback2d
	real, dimension(:,:,:), allocatable			:: phi3d, velback3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("vsalt",vsalt,4500.0)
	call from_aux("velback", "n1", nz)
	call from_aux("velback", "n2", nx)
	call from_aux("velback", "n3", ny,1)
	call from_aux("velback",'o1', oz)
	call from_aux("velback",'o2', ox)
	call from_aux("velback",'o3', oy)
	call from_aux("velback",'d1', dz)
	call from_aux("velback",'d2', dx)
	call from_aux("velback",'d3', dy)

	call to_history("n1",nz)
	call to_history("n2",nx)
	call to_history("n3",ny)
	call to_history("o1",oz)
	call to_history("o2",ox)
	call to_history("o3",oy)
	call to_history("d1",dz)
	call to_history("d2",dx)
	call to_history("d3",dy)

	if (ny==1) then
		if(verbose) write(0,*) "========= Detected 2D inputs ==============="
		allocate(phi2d(nz, nx))
		allocate(velback2d(nz, nx))
		call sep_read(phi2d,"phi")
		call sep_read(velback2d,"velback")
		if(verbose) write(0,*) "minval(velback2d) = ", minval(velback2d)
		WHERE (phi2d >0.0) velback2d = vsalt
		if(verbose) write(0,*) "minval(velback2d) = ", minval(velback2d)
		if(verbose) write(0,*) "maxval(velback2d) = ", maxval(velback2d)
		call sep_write(velback2d)
	else
		if(verbose) write(0,*) "========= Detected 3D inputs ==============="
		allocate(phi3d(nz, nx, ny))
		allocate(velback3d(nz, nx, ny))
		call sep_read(phi3d,"phi")
		call sep_read(velback3d,"velback")
		WHERE (phi3d >0.0) velback3d = vsalt
		if(verbose) write(0,*) "maxval(velback3d) = ", maxval(velback3d)
		call sep_write(velback3d)
	end if

end program

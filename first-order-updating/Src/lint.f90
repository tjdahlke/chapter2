module lint_mod

  implicit none

  integer, private                         :: n1, n2, nt, nt2, wfld_j, iw, it, it2
  real, private                            :: c
  real, dimension(:), allocatable, private :: wt1, wt2

  contains

  subroutine lint_init(n1_in, n2_in, nt_in, wfld_j_in)
    integer                     :: n1_in, n2_in, nt_in, wfld_j_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    wfld_j = wfld_j_in
    nt2 = (nt-1)*wfld_j + 1
    if(.not. allocated(wt1)) allocate(wt1(wfld_j))
    if(.not. allocated(wt2)) allocate(wt2(wfld_j))
    do iw=1,wfld_j
      wt2(iw) = 1.*iw/wfld_j
    end do
    wt1 = 1. - wt2
!    c = sqrt(2.*sum(wt2*wt2)-1.)
    c = sqrt(1.*wfld_j)
    wt1 = wt1 / c
    wt2 = wt2 / c
  end subroutine

  function lint_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call lint_op2(adj, add, model, data)
    stat=0
  end function

  subroutine lint_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add
    real, dimension(n1,n2,nt)   :: model
    real, dimension(n1,n2,nt2)  :: data
    if(adj) then
      if(.not. add) model = 0.
      model(:,:,1) = model(:,:,1) + data(:,:,1) / c
      !$OMP DO
      do it=1,nt-1
        do iw=1,wfld_j
          it2 = (it-1)*wfld_j+1+iw
          model(:,:,it) = model(:,:,it) + wt1(iw)*data(:,:,it2)
          model(:,:,it+1) = model(:,:,it+1) + wt2(iw)*data(:,:,it2)
        end do
      end do
      !$OMP END DO
    else
      if(.not. add) data = 0.
      data(:,:,1) = data(:,:,1) + model(:,:,1) / c
      !$OMP DO
      do it=1,nt-1
        do iw=1,wfld_j
          it2 = (it-1)*wfld_j+1+iw
          data(:,:,it2) = data(:,:,it2) + wt1(iw)*model(:,:,it) + wt2(iw)*model(:,:,it+1)
        end do
      end do
      !$OMP END DO
    end if
  end subroutine

end module

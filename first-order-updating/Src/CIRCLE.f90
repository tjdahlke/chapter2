
!===============================================================================
!		Places an elipse of given dimensions on a file
!		MODEL:  Nz x Nx
!		DATA:  	Nz x Nx
!===============================================================================

program CIRCLE
	use sep
	implicit none

	integer								:: nz,nx,iz,ix
	real,dimension(:,:),allocatable		:: data
	real								:: replaceval,radx,radz,centx,centz,ox,dx,oz,dz,x,z,test
	logical								:: verbose

	call sep_init()

	call from_param("verbose",verbose,.TRUE.)
	if(verbose) write(0,*) "==================== Read in initial parameters"
	call from_param("centx",centx)
	call from_param("centz",centz)
	call from_param("radx",radx)
	call from_param("radz",radz)
	call from_param("replaceval",replaceval,-1.0)
	call from_history("n1",nz)
	call from_history("n2",nx)
	call from_history("d1",dz)
	call from_history("d2",dx)
	call from_history("o1",oz)
	call from_history("o2",ox)

	allocate(data(nz,nx))
	data=0.0

	do iz=1,nz
		z = oz + (iz-1)*dz - centz
		do ix=1,nx
			x = ox + (ix-1)*dx -centx
			test = (x/radx)**2 + (z/radz)**2
			if ( test<=1.0 ) then
				data(iz,ix)=replaceval
			end if
		end do
	end do

!##########################################################################################


	if(verbose) write(0,*) "==================== Write out RBF file (implicit surface)"
	call sep_write(data)

	if (verbose) write(0,*) "APPLY_RBF program complete"
	deallocate(data)


end program

module fbi_mod

  use sep
  use mem_mod
  use tomo_mod
  use sscale_mod
  use lint_mod
  use step_mod
  use scats0_mod
  use scats1_mod
  use scatr0_mod
  use escats0_mod
  use escatr0_mod
  use escat_refl_mod
  use secder_mod

  implicit none
  integer, private                                     :: nt, n1, n2, nt2, wfld_j
  integer, private                                     :: sou_nx, sou_ox, sou_dx, sou_z, isou, sou_x
  integer, private                                     :: rec_nx, rec_ox, rec_dx, rec_z, irec, rec_x
  integer, private                                     :: nh, oh, it
  integer, private                                     :: bc1, bc2, bc3, bc4
  logical, private                                     :: usesrc
  real, private                                        :: dt, d1, o1, d2, o2, dtw, pi
  real, dimension(:), allocatable, private             :: wavelet
  real, dimension(:,:), allocatable, target, private   :: vel, veldt2, vwt, wt, owt
  real, dimension(:,:,:), allocatable, private         :: dobs, srcwave
  real, dimension(:,:,:), allocatable, target, private :: wflds0, wflds1, wfldr0, refl
  real, dimension(:,:,:), allocatable, target, private :: temp1, temp2, temp1i, temp2i
  contains

  subroutine init1(n1_in, o1_in, d1_in, n2_in, o2_in, d2_in, bc1_in, bc2_in, bc3_in, bc4_in)
    integer                             :: n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in
    real                                :: d1_in, o1_in, d2_in, o2_in
    n1 = n1_in
    n2 = n2_in
    d1 = d1_in
    o1 = o1_in
    d2 = d2_in
    o2 = o2_in
    bc1 = bc1_in
    bc2 = bc2_in
    bc3 = bc3_in
    bc4 = bc4_in
  end subroutine

  subroutine init2(sou_nx_in, sou_ox_in, sou_dx_in, sou_z_in, rec_nx_in, rec_ox_in, rec_dx_in, rec_z_in)
    integer                             :: sou_nx_in, sou_ox_in, sou_dx_in, sou_z_in
    integer                             :: rec_nx_in, rec_ox_in, rec_dx_in, rec_z_in
    sou_nx = sou_nx_in
    sou_ox = sou_ox_in
    sou_dx = sou_dx_in
    sou_z = sou_z_in
    rec_nx = rec_nx_in
    rec_ox = rec_ox_in
    rec_dx = rec_dx_in
    rec_z = rec_z_in
  end subroutine

  subroutine init3(nt_in, dt_in, wfld_j_in, nh_in, oh_in)
    integer                             :: nt_in, wfld_j_in, nh_in, oh_in
    real                                :: dt_in
    nt = nt_in
    wfld_j = wfld_j_in
    dt = dt_in
    nh = nh_in
    oh = oh_in
  end subroutine

  subroutine init_alloc(wavelet_in)
    real, dimension(nt)                  :: wavelet_in

    nt2 = (nt-1)*wfld_j+1
    dtw = dt / wfld_j

    call mem_alloc1d(wavelet, nt)
    call mem_alloc2d(vel, n1, n2)
    call mem_alloc2d(veldt2, n1, n2)
    call mem_alloc2d(vwt, n1, n2)
    call mem_alloc2d(wt, n1, n2)
    call mem_alloc3d(wflds0, n1, n2, nt)
    call mem_alloc3d(wflds1, n1, n2, nt)
    call mem_alloc3d(wfldr0, n1, n2, nt)
    call mem_alloc3d(refl, n1, n2, nh)
    ! call mem_alloc3d(dobs, nt, rec_nx, sou_nx)
    call mem_alloc3d(dobs, nt, rec_nx, 1)
    call mem_alloc3d(temp1, n1, n2, nt)
    call mem_alloc3d(temp2, n1, n2, nt)
    call mem_alloc3d(temp1i, n1, n2, nt2)
    call mem_alloc3d(temp2i, n1, n2, nt2)
    ! call mem_alloc2d(owt, rec_nx, sou_nx)

    call mem_alloc3d(srcwave, n1, n2, nt)

    usesrc=.false.
    wavelet = wavelet_in

    call sscale_init(n1, n2, nt, dtw, veldt2, wt)
    call lint_init(n1, n2, nt, wfld_j)
    call step_init(n1, n2, nt2, bc1, bc2, bc3, bc4, d1, d2, veldt2, vwt, wt)
    call scats0_init(n1, n2, nt, wflds0)
    call scats1_init(n1, n2, nt, wflds1)
    call scatr0_init(n1, n2, nt, wfldr0)
    call escats0_init(n1, n2, nt, nh, oh, wflds0)
    call escatr0_init(n1, n2, nt, nh, oh, wfldr0)
    call escat_refl_init(n1, n2, nt, nh, oh, refl)
    call secder_init(n1, n2, nt, dt)

    ! pi = 3.14159265358979323846
    ! do isou=1,sou_nx
    !   sou_x = (isou-1)*sou_dx + sou_ox
    !   do irec=1,rec_nx
    !     rec_x = (irec-1)*rec_dx + rec_ox
    !     owt(irec, isou) = -1.*abs(sou_x-rec_x)
    !   end do
    ! end do
    ! owt = owt - minval(owt) + 1.
    ! owt = sin(pi/2.*owt/maxval(owt))*sin(pi/2.*owt/maxval(owt))
  end subroutine


  subroutine model_update(vel_in)
    real, dimension(n1, n2)              :: vel_in
    vel = 0.
    vwt = 0.
    wt = 0.
    vel = vel_in
    call AbcWt(vel, vwt, wt, dtw, d1, d2, bc1, bc2, bc3, bc4)
    veldt2 = vel*vel*dtw*dtw
  end subroutine


  subroutine refl_update(refl_in)
    real, dimension(n1, n2, nh)          :: refl_in
    refl = refl_in
  end subroutine


  subroutine data_update(dobs_in)
    ! real, dimension(nt, rec_nx, sou_nx)  :: dobs_in
    real, dimension(nt, rec_nx, 1)  :: dobs_in
    dobs = dobs_in
  end subroutine


  subroutine init_srcwave(srcwave_in)
    real, dimension(n1,n2,nt)  :: srcwave_in
    srcwave = srcwave_in
    usesrc=.true.
    write(0,*) ">>>>> READ IN THE SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
  end subroutine



  function L_nlop(adj, add, model, data) result(stat)
    integer             :: stat
    logical, intent(in) :: adj, add
    real, dimension(:)  :: model, data
    call L_nlop2(adj, add, model, data, 1)
    stat=0
  end function

  function L_lop(adj, add, model, data) result(stat)
    integer             :: stat
    logical, intent(in) :: adj, add
    real, dimension(:)  :: model, data
    call L_lop2(adj, add, model, data, 1)
    stat=0
  end function

  function T_lop(adj, add, model, data) result(stat)
    integer             :: stat
    logical, intent(in) :: adj, add
    real, dimension(:)  :: model, data
    call T_lop2(adj, add, model, data, 1)
    stat=0
  end function

  function W_lop(adj, add, model, data) result(stat)
    integer             :: stat
    logical, intent(in) :: adj, add
    real, dimension(:)  :: model, data
    call W_lop2(adj, add, model, data, 1)
    stat=0
  end function


!===============================================================================
  subroutine L_nlop2(adj, add, model, data, isou)
    logical, intent(in)                  :: adj, add
    ! real, dimension(nt, rec_nx, sou_nx)  :: model
    ! real, dimension(nt, rec_nx, sou_nx)  :: data
    real, dimension(nt, rec_nx, 1)       :: model
    real, dimension(nt, rec_nx, 1)       :: data
    integer                              :: isou

    if(adj) then
      if(.not. add) model = 0.
      ! do isou=1,sou_nx
        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = data(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = data(:,irec,isou)*owt(irec,isou)
        end do
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp2, temp1i)
        call sscale_op2(.true., .false., temp1, temp2)
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          model(:,irec,1) = model(:,irec,1) + temp1(sou_z,rec_x,:)
          ! model(:,irec,isou) = model(:,irec,isou) + temp1(sou_z,rec_x,:)
        end do
      ! end do
    else
      if(.not. add) data = 0.
      ! do isou=1,sou_nx
        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(sou_z,rec_x,:) = model(:,irec,1)
          ! temp1(sou_z,rec_x,:) = model(:,irec,isou)
        end do

        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp2i)

        if (exist_file('src_wave')) then
          call to_history("n1",n1, 'src_wave')
          call to_history("n2",n2, 'src_wave')
          call to_history("n3",nt, 'src_wave')
          call to_history("d1",d1, 'src_wave')
          call to_history("d2",d2, 'src_wave')
          call to_history("d3",dt, 'src_wave')
          call to_history("o1",o1, 'src_wave')
          call to_history("o2",o2, 'src_wave')
          call to_history("o3",0.0, 'src_wave')
          call to_history("label1",'z [km]', 'src_wave')
          call to_history("label2",'x [km]', 'src_wave')
          call sep_write(temp1,	'src_wave')
        end if

        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          data(:,irec,1) = data(:,irec,1) + temp1(rec_z,rec_x,:)!*owt(irec,isou)
          ! data(:,irec,isou) = data(:,irec,isou) + temp1(rec_z,rec_x,:)*owt(irec,isou)
        end do
      ! end do
    end if
  end subroutine

!===============================================================================
  subroutine L_lop2(adj, add, model, data, isou)
    logical, intent(in)                  :: adj, add
    real, dimension(n1, n2, nh)          :: model
    ! real, dimension(nt, rec_nx, sou_nx)  :: data
    real, dimension(nt, rec_nx, 1)       :: data
    integer                              :: isou

    if(adj) then
      if(.not. add) model = 0.
      !do isou=1,sou_nx

      if (.not. usesrc) then
        write(0,*) ">>>>> RE-COMPUTING SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)
      else
        write(0,*) ">>>>> USING LOADED SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        wflds0 = srcwave
      endif

        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = data(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = data(:,irec,isou)*owt(irec,isou)
        end do
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp1i)
        call sscale_op2(.true., .false., temp2, temp1)
        call secder_op2(.true., .false., temp1, temp2)
        call escats0_op2(.true., .true., model, temp1)
      ! end do

        if (exist_file('rec_wave_out')) then
          call to_history("n1",n1, 'rec_wave_out')
          call to_history("n2",n2, 'rec_wave_out')
          call to_history("n3",nt, 'rec_wave_out')
          call to_history("d1",d1, 'rec_wave_out')
          call to_history("d2",d2, 'rec_wave_out')
          call to_history("d3",dt, 'rec_wave_out')
          call to_history("o1",o1, 'rec_wave_out')
          call to_history("o2",o2, 'rec_wave_out')
          call to_history("o3",0.0, 'rec_wave_out')
          call to_history("label1",'z [km]', 'rec_wave_out')
          call to_history("label2",'x [km]', 'rec_wave_out')
          call sep_write(temp1,	'rec_wave_out')
        end if

    else
      if(.not. add) data = 0.
      !do isou=1,sou_nx

      if (.not. usesrc) then
        write(0,*) ">>>>> RE-COMPUTING SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)
      else
        write(0,*) ">>>>> USING LOADED SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        wflds0 = srcwave
      endif

        call escats0_op2(.false., .false., model, temp1)
        call secder_op2(.false., .false., temp1, temp2)
        call sscale_op2(.false., .false., temp2, temp1)
        call lint_op2(.false., .false., temp1, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp2i)
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          data(:,irec,1) = data(:,irec,1) + temp1(rec_z,rec_x,:)!*owt(irec,isou)
          ! data(:,irec,isou) = data(:,irec,isou) + temp1(rec_z,rec_x,:)*owt(irec,isou)
        end do
      ! end do

      if (exist_file('scat_wave_out')) then
        call to_history("n1",n1, 'scat_wave_out')
        call to_history("n2",n2, 'scat_wave_out')
        call to_history("n3",nt, 'scat_wave_out')
        call to_history("d1",d1, 'scat_wave_out')
        call to_history("d2",d2, 'scat_wave_out')
        call to_history("d3",dt, 'scat_wave_out')
        call to_history("o1",o1, 'scat_wave_out')
        call to_history("o2",o2, 'scat_wave_out')
        call to_history("o3",0.0, 'scat_wave_out')
        call to_history("label1",'z [km]', 'scat_wave_out')
        call to_history("label2",'x [km]', 'scat_wave_out')
        call sep_write(temp1,	'scat_wave_out')
      end if

    end if
  end subroutine

!===============================================================================
  subroutine T_lop2(adj, add, model, data, isou)
    logical, intent(in)                  :: adj, add
    real, dimension(n1, n2)              :: model
    ! real, dimension(nt, rec_nx, sou_nx)  :: data
    real, dimension(nt, rec_nx, 1)       :: data

    integer                              :: isou

    if(adj) then
      if(.not. add) model = 0.
      !do isou=1,sou_nx
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)

        call escats0_op2(.false., .false., refl, temp1)
        call secder_op2(.false., .false., temp1, temp2)
        call sscale_op2(.false., .false., temp2, temp1)
        call lint_op2(.false., .false., temp1, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds1, temp2i)

        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = data(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = data(:,irec,isou)*owt(irec,isou)
        end do
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp1i)
        call sscale_op2(.true., .false., temp2, temp1)
        call secder_op2(.true., .false., temp1, temp2)
        call scats1_op2(.true., .true., model, temp1)

        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = data(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = data(:,irec,isou)*owt(irec,isou)
        end do
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp2, temp1i)
        call sscale_op2(.true., .false., temp1, temp2)
        call secder_op2(.true., .false., temp2, temp1)
        call escat_refl_op2(.true., .false., temp1, temp2)
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp1i)
        call sscale_op2(.true., .false., temp2, temp1)
        call secder_op2(.true., .false., temp1, temp2)
        call scats0_op2(.true., .true., model, temp1)
      ! end do
    else
      if(.not. add) data = 0.
      !do isou=1,sou_nx
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)

        call escats0_op2(.false., .false., refl, temp1)
        call secder_op2(.false., .false., temp1, temp2)
        call sscale_op2(.false., .false., temp2, temp1)
        call lint_op2(.false., .false., temp1, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds1, temp2i)

        call scats1_op2(.false., .false., model, temp1)
        call secder_op2(.false., .false., temp1, temp2)
        call sscale_op2(.false., .false., temp2, temp1)
        call lint_op2(.false., .false., temp1, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp2i)
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          data(:,irec,1) = data(:,irec,1) + temp1(rec_z,rec_x,:)!*owt(irec,isou)
          ! data(:,irec,isou) = data(:,irec,isou) + temp1(rec_z,rec_x,:)*owt(irec,isou)
        end do

        call scats0_op2(.false., .false., model, temp1)
        call secder_op2(.false., .false., temp1, temp2)
        call sscale_op2(.false., .false., temp2, temp1)
        call lint_op2(.false., .false., temp1, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp2i)
        call escat_refl_op2(.false., .false., temp1, temp2)
        call secder_op2(.false., .false., temp2, temp1)
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp1, temp2i)
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          data(:,irec,1) = data(:,irec,1) + temp1(rec_z,rec_x,:)!*owt(irec,isou)
          ! data(:,irec,isou) = data(:,irec,isou) + temp1(rec_z,rec_x,:)*owt(irec,isou)
        end do
      ! end do
    end if
  end subroutine

!===============================================================================
  subroutine W_lop2(adj, add, model, data, isou)
    logical, intent(in)          :: adj, add
    real, dimension(n1, n2)      :: model
    real, dimension(n1, n2, nh)  :: data
    integer                      :: isou

    if(adj) then
      if(.not. add) model = 0.
      !do isou=1,sou_nx

      if (.not. usesrc) then
        write(0,*) ">>>>> RE-COMPUTING SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)
      else
        write(0,*) ">>>>> USING LOADED SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        wflds0 = srcwave
      endif

        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = dobs(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = dobs(:,irec,isou)*owt(irec,isou)
        end do

        !  Propagating the residual wavefield
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp2, temp1i)
        call sscale_op2(.true., .false., temp1, temp2)
        call secder_op2(.true., .false., wfldr0, temp1)

        call escatr0_op2(.false., .false., data, temp1);
        call lint_op2(.false., .false., temp1, temp2i);
        call step_op2(.true., .false., temp1i, temp2i);
        call lint_op2(.true., .false., temp2, temp1i);
        call sscale_op2(.true., .false., temp1, temp2);
        call secder_op2(.true., .false., temp2, temp1);
        call scats0_op2(.true., .true., model, temp2);

        call escats0_op2(.false., .false., data, temp1);
        call secder_op2(.false., .false., temp1, temp2);
        call sscale_op2(.false., .false., temp2, temp1);
        call lint_op2(.false., .false., temp1, temp1i);
        call step_op2(.false., .false., temp1i, temp2i);
        call lint_op2(.true., .false., temp1, temp2i);
        call scatr0_op2(.true., .true., model, temp1);
      ! end do
    else
      if(.not. add) data = 0.
      !do isou=1,sou_nx

      if (.not. usesrc) then
        write(0,*) ">>>>> RE-COMPUTING SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        sou_x = (isou-1)*sou_dx + sou_ox
        temp1 = 0.
        temp1(sou_z,sou_x,:) = wavelet
        call sscale_op2(.false., .false., temp1, temp2)
        call lint_op2(.false., .false., temp2, temp1i)
        call step_op2(.false., .false., temp1i, temp2i)
        call lint_op2(.true., .false., wflds0, temp2i)
      else
        write(0,*) ">>>>> USING LOADED SRCWAVE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        wflds0 = srcwave
      endif

        temp1 = 0.
        do irec=1,rec_nx
          rec_x = (irec-1)*rec_dx + rec_ox
          temp1(rec_z,rec_x,:) = dobs(:,irec,1)!*owt(irec,isou)
          ! temp1(rec_z,rec_x,:) = dobs(:,irec,isou)*owt(irec,isou)
        end do

        !  Propagating the residual wavefield
        call lint_op2(.false., .false., temp1, temp2i)
        call step_op2(.true., .false., temp1i, temp2i)
        call lint_op2(.true., .false., temp2, temp1i)
        call sscale_op2(.true., .false., temp1, temp2)
        call secder_op2(.true., .false., wfldr0, temp1)

        call scatr0_op2(.false., .false., model, temp1);
        call lint_op2(.false., .false., temp1, temp2i);
        call step_op2(.true., .false., temp1i, temp2i);
        call lint_op2(.true., .false., temp2, temp1i);
        call sscale_op2(.true., .false., temp1, temp2);
        call secder_op2(.true., .false., temp2, temp1);
        call escats0_op2(.true., .true., data, temp2);

        call scats0_op2(.false., .false., model, temp1);
        call secder_op2(.false., .false., temp1, temp2);
        call sscale_op2(.false., .false., temp2, temp1);
        call lint_op2(.false., .false., temp1, temp1i);
        call step_op2(.false., .false., temp1i, temp2i);
        call lint_op2(.true., .false., temp1, temp2i);
        call escatr0_op2(.true., .true., data, temp1);
      ! end do
    end if
  end subroutine

end module
